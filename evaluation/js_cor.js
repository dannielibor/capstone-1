$(document).ready(function() {
	$("#sample li").click(function() { 
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"get-cor.php",
				data:{cor:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#crs").html(resp);
				}
			});
			
		} else {
			$("#crs").html("<option value='' disabled selected>Please choose...</option>");
		}
		
	});
});
