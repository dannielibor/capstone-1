<?php
session_start();
include_once'includes/db.inc.php';

?>
<!DOCTYPE html>
<html>

	<head>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<link rel="stylesheet" type="text/css" href="style.css">
		<title>SBCA - Evaluation Form</title>
		
		
		<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
       
	  var data = new google.visualization.DataTable();
	  
          data.addColumn('string','');
		  data.addColumn('number','Departments');
		  
		  <?php
		  
		  $select = "select * from tblcor group by department asc";
		  $res = mysqli_query($conn,$select);
		  
		  while($row = mysqli_fetch_assoc($res)){
			  
			  $dept = "select * from tblcor where department = '".$row['department']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."'";
			  $set = mysqli_query($conn,$dept);
			  
			  $alldepts = array();
			  
			  while($get = mysqli_fetch_assoc($set)){
			  
			  $query1 = "select * from tblsummary where facultyID = '".$get['facultyID']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."'";
			  $q1 = mysqli_query($conn,$query1);
			  $r1 = mysqli_fetch_assoc($q1);
			  
			  if($r1 > 1){
			  
			  
			  $query2 = "select * from tblappraisal where appraisalID = '".$r1['appraisalID']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."'";
			  $q2 = mysqli_query($conn,$query2);
			  $r2 = mysqli_fetch_assoc($q2);
			  
			  $query3 = "select * from tbldeanappraisal where deanAppraisalID = '".$r2['deanAppraisalID']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."'";
			  $q3 = mysqli_query($conn,$query3);
			  $r3 = mysqli_fetch_assoc($q3);
			  
			  $deanApp = $r3['total'];
			  
			  $query4 = "select * from tblviceappraisal where viceAppraisalID = '".$r2['viceAppraisalID']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."'";
			  $q4 = mysqli_query($conn,$query4);
			  $r4 = mysqli_fetch_assoc($q4);
			  
			  $viceApp = $r4['total'];
			  
			  $query5 = "select * from tblchairappraisal where chairAppraisalID = '".$r2['chairAppraisalID']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."'";
			  $q5 = mysqli_query($conn,$query5);
			  $r5 = mysqli_fetch_assoc($q5);
			  
			  $chairApp = $r5['total'];
			  
			  $avgApp = ((($deanApp + $viceApp) / 2) * 0.20) + ($chairApp * 0.20);
			  $avgApp = number_format((float)$avgApp, 2, '.', '');
			  //appraisal
			  
			  $query6 = "select * from tblclassobserve where classID = '".$r1['classID']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."'";
			  $q6 = mysqli_query($conn,$query6);
			  $r6 = mysqli_fetch_assoc($q6);
			  
			  $query7 = "select * from tbldeanobservation where deanObsID = '".$r6['deanObsID']."'";
			  $q7 = mysqli_query($conn,$query7);
			  $r7 = mysqli_fetch_assoc($q7);
			  
			  $deanObs = $r7['total'] / 21;
			  
			  $query8 = "select * from tblviceobservation where viceObsID = '".$r6['viceObsID']."'";
			  $q8 = mysqli_query($conn,$query8);
			  $r8 = mysqli_fetch_assoc($q8);
			  
			  $viceObs = $r8['total'] / 21;
			  
			  $query9 = "select * from tblchairobservation where chairObsID = '".$r6['chairObsID']."'";
			  $q9 = mysqli_query($conn,$query9);
			  $r9 = mysqli_fetch_assoc($q9);
			  
			  $chairObs = $r9['total'] / 21;
			  
			  $avgObs = ($viceObs * 0.05) + ($chairObs * 0.30);
			  $avgObs = number_format((float)$avgObs, 2, '.', '');
			  //observation 
			  
			  $array = array();
			  $query10 = "select * from tblcor where facultyID = '".$r1['facultyID']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."'";
			  $q10 = mysqli_query($conn,$query10);
			  while($r10 = mysqli_fetch_assoc($q10)){
				  $query11 = "select * from tblstudeval where regID = '".$r10['regID']."' and facultyID = '".$r1['facultyID']."'";
				  $q11 = mysqli_query($conn,$query11);
				  $r11 = mysqli_fetch_assoc($q11);
				  
				  array_push($array,$r11['total']);
					
			  }
			  $array = array_filter($array);
			  if(count($array) == 0){
			  $stud = 0.00; 
			  }else{
			   $stud = array_sum($array)/count($array);
			  $stud = $stud * 0.20; 
			 
			  }
			  //studevaluation
			  
			  $query12 = "select * from tblselfeval where selfevalID = '".$r1['selfevalID']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."'";
			  $q12 = mysqli_query($conn,$query12);
			  $r12 = mysqli_fetch_assoc($q12);
			  
			  $self = $r12['total'] * 0.05; 
			  //self evaluation
			  
			  $all = $self + $stud + $avgObs + $avgApp;
			  
			  array_push($alldepts,$all);
			  
		  
			  }
		  
		  }
		  
		  if(count($alldepts) != 0){
			  $alldepts = array_filter($alldepts);
			  $alldepts = array_sum($alldepts)/count($alldepts);
if($row['department'] == "Religious Studies and Philisophy"){
echo "data.addRows([
				  ['RSP',".$alldepts."]
				  ]);";
}else{
echo "data.addRows([
				  ['".$row['department']."',".$alldepts."]
				  ]);";
}
			  
		  }else{
			  if($row['department'] == "Religious Studies and Philisophy"){
echo "data.addRows([
				  ['RSP',0]
				  ]);";
}else{
echo "data.addRows([
				  ['".$row['department']."',0]
				  ]);";
}
		  }
		  
		   
		  
	  }
		  ?>
		  

        var options = {
          chart: {
            title: 'Faculty Performance Appraisal Summative Report',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material1'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
	
	
	<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
       
	  var data = new google.visualization.DataTable();
	  
          data.addColumn('string','');
		   data.addColumn('number','Departments');
		 
		  
		<?php
		  
		  $select = "select * from tblcor group by department asc";
		  $res = mysqli_query($conn,$select);
		  
		  while($row = mysqli_fetch_assoc($res)){
			  
			  $dept = "select * from tblcor where department = '".$row['department']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."'";
			  $set = mysqli_query($conn,$dept);
			  
			  $alldepts = array();
			  
			  while($get = mysqli_fetch_assoc($set)){
			  
			  $query1 = "select * from tblsummary where facultyID = '".$get['facultyID']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."'";
			  $q1 = mysqli_query($conn,$query1);
			  $r1 = mysqli_fetch_assoc($q1);
			  
			  if($r1 > 1){
			  
			  $array = array();
			  $query10 = "select * from tblcor where facultyID = '".$r1['facultyID']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."'";
			  $q10 = mysqli_query($conn,$query10);
			  while($r10 = mysqli_fetch_assoc($q10)){
				  $query11 = "select * from tblstudeval where regID = '".$r10['regID']."' and facultyID = '".$r1['facultyID']."'";
				  $q11 = mysqli_query($conn,$query11);
				  $r11 = mysqli_fetch_assoc($q11);
				  
				  array_push($array,$r11['total']);
					
			  }
			  $array = array_filter($array);
			  if(count($array) == 0){
			  $stud = 0.00; 
			  }else{
			   $stud = array_sum($array)/count($array);
			  }
			  //studevaluation
			  
			  $all = $stud;
			  
			  array_push($alldepts,$all);
			  
		  
			  }
		  
		  }
		  
		  if(count($alldepts) != 0){
			  $alldepts = array_filter($alldepts);
			  $alldepts = array_sum($alldepts)/count($alldepts);
if($row['department'] == "Religious Studies and Philisophy"){
echo "data.addRows([
				  ['RSP',".$alldepts."]
				  ]);";
}else{
echo "data.addRows([
				  ['".$row['department']."',".$alldepts."]
				  ]);";
}
		  }else{
			  if($row['department'] == "Religious Studies and Philisophy"){
echo "data.addRows([
				  ['RSP',0]
				  ]);";
}else{
echo "data.addRows([
				  ['".$row['department']."',0]
				  ]);";
}
		  }
		  
		   
		  
	  }
		  ?>

        var options = {
          chart: {
            title: "Student's Evaluation",
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material2'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
		
		
		
	</head>
	
		<body>
		
			<div id="main">
			
			<div id="columnchart_material1" style="position:absolute;width: 800px; height: 500px;margin-top:10%;margin-left:25%;"></div>
			<div id="columnchart_material2" style="position:absolute;width: 800px; height: 500px;margin-top:50%;margin-left:25%;"></div>
		
			<header>
			
				<div class = "container">
				
					<div class = "asd">
					
						<div id="mySidenav" class="sidenav">
						
							<img src="background.jpg" height = "100" width = "100" class = "picture" style="cursor:pointer">
							<a href = "dean.php" style = "font-size:13px">Home</a>
							<!--<a href = "profile.php" style = "font-size:13px">Profile</a>!-->
							<a href = "departments.php?eval=performanceAppraisal&val=1" style = "font-size:13px">Faculty Performance Appraisal</a>
							<a href = "professors.php?eval=DeptperformanceAppraisalVice&val=1" style = "font-size:13px">Department Chair Performance Appraisal</a>
							<a href = "viceappraisalDean.php" style = "font-size:13px">Vice Dean Performance Appraisal</a>
							<a href = "departments.php?eval=classobserve&val=1" style = "font-size:13px">Faculty Classroom Observation</a>
							<a href = "professors.php?eval=deptclassobserve&val=1" style = "font-size:13px">Department Chair Classroom Observation</a>
							<a href = "viceobserveDean.php" style = "font-size:13px">Vice Dean Classroom Observation</a>
							<a href = "statDean.php" style = "font-size:13px">Statistics</a>
							<!--<a href = "stats.php" style = "font-size:13px">Department Chair Statistics</a>!-->
							<!--<a href = "settings.php" style = "font-size:13px">Settings</a>!-->
							<a onclick = "document.getElementById('id02').style.display='block'" style = "cursor:pointer;font-size:13px">Logout</a>
						
						</div>
						
						<img src="btnNav.jpg" alt="Navigation" height = "85dp" class = "menu" onclick="openNav()" style="cursor:pointer">
		
						<a href = "account.php"><img src="logo.png" alt="San Beda College Alabang" height = "70dp" class = "logo" style="cursor:pointer"></a>
					
						<font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">San Beda College Alabang</font>
						
						
					</div>
					
					
						<nav class = "nav">
							<ul class = "ul">
								<li class = "li"><a class = "a" href = "#"><?php echo $_SESSION['firstname'] ?></a></li>
							</ul>
						</nav>
						
					<div class = "eval" style = "margin-top:10%;position:fixed;height:30%;margin-left:-0.1px;width:15%;text-align:left">
				
			<label>RESPONSE CODE:</label><br><br>
<label>5 = Outstanding</label><br><br>
<label>4 = Very Satisfactory</label><br><br>
<label>3 = Satisfactory</label><br><br>
<label>2 = Moderate Satisfactory</label><br><br>
<label>1 = Needs Improvement</label>			
			
			</div>
					
					
				</div>
				
			</div>
			
			</header>
	
			<footer class = "footerhome">
			
				<div class = "follow">
				
					<p class = "bottom">FOLLOW US</p>
					
					<ul class = "link">
					
						<li><a href="https://www.google.com.ph/?gfe_rd=cr&dcr=0&ei=12WNWqO6G9GiX_j3gqgJ" target="_blank">
							<image src = "google.png" height = "10"> Google+</a></li><br>
						
						<li><a href="https://twitter.com/sbcasec?lang=en" target="_blank">
							<image src = "twitter.png" height = "10"> Twitter</a></li><br>
							
						<li><a href="https://www.facebook.com/BedaAlabang/" target="_blank">
							<image src = "facebook.png" height = "10"> Facebook</a></li><br>
							
						<li><a href="https://www.instagram.com/?hl=en" target="_blank">
							<image src = "instagram.png" height = "10"> Instagram</a></li><br>
							
						<li><a href="https://www.youtube.com" target="_blank"><image src = "youtube.png" height = "10"> Youtube</a></li>
					
					</ul>
					
				</div>
				
				<div class = "contactUs">
				
					<p class = "bottom">CONTACT US</p>
					
					<ul class = "link">
					
						<li><a class = "aaaa" href="http://www.sanbeda-alabang.edu.ph/bede/" target="_blank">
							Website: http://www.sanbeda-alabang.edu.ph</a></li><br>
							
						<li><a class = "aaaa" href="#home" style = "cursor:auto">E-mail: sbca@sanbeda-alabang.edu.ph</a></li><br>
						
						<li><a class = "aaaa" href="#home" style = "cursor:auto">Telephone: 236-7222</a></li><br>
					
					</ul>
					
				</div>
				
				<div class = "copyright">
					<p id = "copyright"/>
				</div>
	
			</footer>
			
	<script>
		var d = new Date().getFullYear();
		document.getElementById("copyright").innerHTML = "Copyright &copy; " + d + " San Beda College Alabang, All Rights Reserved.";
	</script>
	
			<div id="id02" class="logout">
  
				<form class="logout-content animate" method = "post" action = "includes/logout.php">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('id02').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
						<label for="username"><b class = "align">Are you sure?</b></label><br>
						<button class="button1" name = "logout"><span>Logout </span></button>
					</div>
			</div>

	<script>
	
		var logout = document.getElementById('id02');
		window.onclick = function(event) {
			if (event.target == logout) {
				logout.style.display = "none";
			}
		}
		
		var x = true;
		function openNav() {
			if(x == true) {
				document.getElementById("mySidenav").style.width = "197px";
				document.getElementById("main").style.marginLeft = "197px";
				x = false;
			}
			else {
				closeNav();
				x = true;
			}
		}

		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
			document.getElementById("main").style.marginLeft= "0";
			document.body.style.backgroundColor = "white";
		}
		
	</script>

		</body>
		
</html>