$(document).ready(function() {
	$("#post").change(function() {
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"get-post.php",
				data:{c_id:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#text").html(resp);
				}
			});
			
		}
		
	});
});
