<?php include("includes/db.inc.php"); session_start();?>
<?php

if(isset($_POST['sum'])) {
	
  $_SESSION['selYR'] = mysqli_real_escape_string($conn, $_POST['sum']);
  
  $sql = "select * from tblcor where schlyr = '".$_SESSION['selYR']."' group by sem asc";
  $res = mysqli_query($conn, $sql);
  if(mysqli_num_rows($res) > 0) {
    echo "<option value='' disabled selected>Please choose...</option>";
    while($row = mysqli_fetch_object($res)) { 
      echo "<option value='".$row->schlyr."'>".$row->sem."</option>";
    }
  }
} else {
  echo "<option value='' disabled selected>Please choose...</option>";
}
?>
 <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['facultyID', 'Summary'],
		  
		  <?php
	
		  $query1 = "select * from tblsummary where facultyID = '".$_SESSION['facid']."' and schlyr = '".$_SESSION['selYR']."' and sem = '".$_SESSION['sem']."'";
		  $q1 = mysqli_query($conn,$query1);
		  $r1 = mysqli_fetch_assoc($q1);
		  
		  $query2 = "select * from tblappraisal where appraisalID = '".$r1['appraisalID']."' and schlyr = '".$_SESSION['selYR']."' and sem = '".$_SESSION['sem']."'";
		  $q2 = mysqli_query($conn,$query2);
		  $r2 = mysqli_fetch_assoc($q2);
		  
		  $query3 = "select * from tblselfeval where selfevalID = '".$r1['selfevalID']."' and schlyr = '".$_SESSION['selYR']."' and sem = '".$_SESSION['sem']."'";
		  $q3 = mysqli_query($conn,$query3);
		  $r3 = mysqli_fetch_assoc($q3);
		  
		  $query4 = "select * from tblclassobserve where classID = '".$r1['classID']."' and schlyr = '".$_SESSION['selYR']."' and sem = '".$_SESSION['sem']."'";
		  $q4 = mysqli_query($conn,$query4);
		  $r4 = mysqli_fetch_assoc($q4);
		  
		  $avg = 0.00;
		  
		  $eval = "select * from tblstudeval where facultyID = '".$r1['facultyID']."'";
		  $reval = mysqli_query($conn,$eval);
		  while($roweval = mysqli_fetch_assoc($reval)){
		  $cor = "select * from tblcor where regID = '".$roweval['regID']."' and schlyr = '".$_SESSION['selYR']."' and sem = '".$_SESSION['sem']."'";
		  $recor = mysqli_query($conn,$cor);
		  $rowcor = mysqli_fetch_assoc($recor);
		  $query5 = "select avg(total) as avg from tblstudeval where facultyID = '".$r1['facultyID']."' and regID = '".$rowcor['regID']."'";
		  $q5 = mysqli_query($conn,$query5);
		  $r5 = mysqli_fetch_assoc($q5);
		  $avg = $avg + $r5['avg'];
		  }
		  echo "['Performance Appraisal',".$r2['total']."],['Self Evaluation',".$r3['total']."],['Classroom Observation',".$r4['total']."],['Student Evaluation',".$avg."]";

		  ?>
          
        ]);

        var options = {
          title: 'FACULTY PERFORMANCE APPRAISAL SUMMATIVE REPORT',
		  backgroundColor: 'none',
		  
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d1'));
        chart.draw(data, options);
      }
    </script>
	
	
	
	<!--end!-->
	
	<script type="text/javascript">
	
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['facultyID', 'Classroom Observation'],
          <?php
		  
		  $query6 = "select * from tblclassobserve where classID = '".$r1['classID']."'";
		  $q6 = mysqli_query($conn,$query6);
		  $r6 = mysqli_fetch_assoc($q6);
		  
		  $query12 = "select * from tblchairobservation where chairObsID = '".$r6['chairObsID']."'";
		  $q12 = mysqli_query($conn,$query12);
		  $r12 = mysqli_fetch_assoc($q12);
		  
		  //$query13 = "select * from tblviceobersvation where viceObsID = '".$r6['viceObsID']."'";
		  //$q13 = mysqli_query($conn,$query13);
		  //$r13 = mysqli_fetch_assoc($q13);
		  
		  echo "['Department Chair','".$r12['total']."'],['Vice Dean','20']";
		  
		  ?>
        ]);

        var options = {
          title: 'CLASSROOM OBSERVATION',
		  backgroundColor:'none',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d2'));
        chart.draw(data, options);
      }
	  
    </script>
	
	<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Instructional Performance', 'Department Chair', 'Vice Dean'],
		  <?php
		  $query7 = "select * from tblclassobserve where classID = '".$r1['classID']."'";
		  $q7 = mysqli_query($conn,$query7);
		  $r7 = mysqli_fetch_assoc($q7);
		  
		  $query8 = "select * from tblchairobservation where chairObsID = '".$r7['chairObsID']."'";
		  $q8 = mysqli_query($conn,$query8);
		  $r8 = mysqli_fetch_assoc($q8);
		  
		  //$query9 = "select * from tblviceobersvation where viceObsID = '".$r7['viceObsID']."'";
		  //$q9 = mysqli_query($conn,$query9);
		  //$r9 = mysqli_fetch_assoc($q9);
		  
		  echo "['A','".$r8['A']."', '5'],['B','".$r8['B']."', '5'],['C','".$r8['C']."', '5'],['D','".$r8['D']."', '5']";
		  ?>
        ]);

        var options = {
          chart: {
            title: 'CLASSROOM OBSERVATION'
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material1'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
	
	<!--end!-->
	
	<script type="text/javascript">
	
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['facultyID', 'Performance Appraisal'],
          <?php
		  
		  $query10 = "select * from tblappraisal where appraisalID = '".$r1['appraisalID']."'";
		  $q10 = mysqli_query($conn,$query10);
		  $r10 = mysqli_fetch_assoc($q10);
		  
		  $query11 = "select * from tblchairappraisal where chairAppraisalID = '".$r10['chairAppraisalID']."'";
		  $q11 = mysqli_query($conn,$query11);
		  $r11 = mysqli_fetch_assoc($q11);
		  
		  $query14 = "select * from tbldeanappraisal where deanAppraisalID = '".$r10['deanAppraisalID']."'";
		  $q14 = mysqli_query($conn,$query14);
		  $r14 = mysqli_fetch_assoc($q14);
		  
		  $query15 = "select * from tblviceappraisal where viceAppraisalID = '".$r10['viceAppraisalID']."'";
		  $q15 = mysqli_query($conn,$query15);
		  $r15 = mysqli_fetch_assoc($q15);
		  
		  echo "['Department Chair','".$r11['total']."'],['Dean','5'],['Vice Dean','5']";
		  
		  ?>
        ]);

        var options = {
          title: 'PERFORMANCE APPRAISAL',
		  backgroundColor:'none',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d3'));
        chart.draw(data, options);
      }
	  
    </script>
	
	<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Faculty Performance Appraisal', 'Department Chair', 'Vice Dean', 'Dean'],
		  <?php
		  $query16 = "select * from tblappraisal where appraisalID = '".$r1['appraisalID']."'";
		  $q16 = mysqli_query($conn,$query16);
		  $r16 = mysqli_fetch_assoc($q16);
		  
		  $query17 = "select * from tblchairappraisal where chairAppraisalID = '".$r16['chairAppraisalID']."'";
		  $q17 = mysqli_query($conn,$query17);
		  $r17 = mysqli_fetch_assoc($q17);
		  
		  $query18 = "select * from tbldeanappraisal where deanAppraisalID = '".$r16['deanAppraisalID']."'";
		  $q18 = mysqli_query($conn,$query18);
		  $r18 = mysqli_fetch_assoc($q18);
		  
		  $query19 = "select * from tblviceappraisal where viceAppraisalID = '".$r16['viceAppraisalID']."'";
		  $q19 = mysqli_query($conn,$query19);
		  $r19 = mysqli_fetch_assoc($q19);
		  
		  echo "['A','".$r17['A']."', '5', '5'],['B','".$r17['B']."', '5', '5'],['C','".$r17['C']."', '5', '5']";
		  ?>
        ]);

        var options = {
          chart: {
            title: 'PERFORMANCE APPRAISAL'
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material2'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
	
	<!--end!-->
	
	<script type="text/javascript">
	
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['facultyID', "Students's Evaluation"],
          <?php
		  
		  $query20 = "select * from tblstudeval where facultyID = '".$r1['facultyID']."'";
		  $q20 = mysqli_query($conn,$query20);
		  
		  while($r20 = mysqli_fetch_assoc($q20)){
			  
		  $query21 = "select * from tblcor where regID = '".$r20['regID']."' and schlyr = '".$_SESSION['selYR']."' and sem = '".$_SESSION['sem']."' and facultyID = '".$_SESSION['facid']."'";
		  $q21 = mysqli_query($conn,$query21);
		  $r21 = mysqli_fetch_assoc($q21);
		  
		  $query22 = "select avg(total) as total from tblstudeval where facultyID = '".$r1['facultyID']."' and regID = '".$r21['regID']."'";
		  $q22 = mysqli_query($conn,$query22);
		  $r22 = mysqli_fetch_assoc($q22);
		  
		  echo "['".$r21['subjectCode']."','".$r22['total']."']";
		  
		  }
		  ?>
        ]);

        var options = {
          title: "STUDENTS' EVALUATION",
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d4'));
        chart.draw(data, options);
      }
	  
    </script>
	
	<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = new google.visualization.DataTable();

data.addColumn('string', '');

var col = "<?php
$sql = "select * from tblcor where facultyID = '".$r1['facultyID']."' and schlyr = '".$_SESSION['selYR']."' and sem = '".$_SESSION['sem']."' group by subjectCode";
$set = mysqli_query($conn,$sql);
while($get = mysqli_fetch_assoc($set)){
	$query = "select * from tblcor where facultyID = '".$r1['facultyID']."' and subjectCode = '".$get['subjectCode']."'  and schlyr = '".$_SESSION['selYR']."' and sem = '".$_SESSION['sem']."' group by yrsec";
	$res = mysqli_query($conn,$query);
	$d = mysqli_fetch_assoc($res);
	
	$query1 = "select avg(A) as A, avg(B) as B, avg(C) as C, avg(D) as D, avg(E) as E from tblstudenteval where regID = '".$d['regID']."'";
	$res1 = mysqli_query($conn,$query1);
	$d1 = mysqli_fetch_assoc($res1);
	
	echo $d['subjectCode']."*A-".$d1['A']."*B-".$d1['B']."*C-".$d1['C']."*D-".$d1['D']."*E-".$d1['E']."/";
}
?>";
col = col.split('/');
var rw;
var rn;
loop:
	for(i = 0; i < col.length; i++){
		if(col[i] == ""){
			break;
		}else{
			rw = col[i].split('*');
			data.addColumn('string', rw[0]);
			for(a = 0; a < rw.length; a++){
				if(rw[1+a] != null){
					rn = rw[1+a].split('-');
					if(rn[1] != ""){
					var txt = rn[0];
					var val = rn[1];
					data.addRows([
					[txt,val]
					]);
					}
					
				}
				}
				
			}
		}

        var options = {
          chart: {
            title: "Student's Evaluation",
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material3'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>