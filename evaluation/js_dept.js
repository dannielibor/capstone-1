$(document).ready(function() {
	$("#subj").change(function() { 
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"get-dept.php",
				data:{dep:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#dep").html(resp);
				}
			});
			
		} else {
			$("#dep").html("<span>Please choose...</span>");
		}
		
	});
});
