$(document).ready(function() {
	$("#sec").change(function() { 
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"get-size.php",
				data:{size:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#size").html(resp);
				}
			});
			
		}
		
	});
});
