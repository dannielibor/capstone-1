$(document).ready(function() {
	$("#sec").change(function() { 
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"get-dates.php",
				data:{date:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#date").html(resp);
				}
			});
			
		}
		
	});
});
