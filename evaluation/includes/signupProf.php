<?php
session_start();
if(isset($_POST['Profsignup'])){
	
	include_once 'db.inc.php';
	
	$profemail   = mysqli_real_escape_string($conn,$_POST['profemail']);
	$profNo   = mysqli_real_escape_string($conn,$_POST['profno']);
    $proffirst  = mysqli_real_escape_string($conn,$_POST['proffirstname']);
    $proflast = mysqli_real_escape_string($conn,$_POST['proflastname']);
    $profuser      = mysqli_real_escape_string($conn,$_POST['profuser']);
    $profpass    = mysqli_real_escape_string($conn,$_POST['profpass']);
	$profgend   = mysqli_real_escape_string($conn, $_POST['profgender']);
$file = addslashes(file_get_contents($_FILES["profile_photoProf"]["tmp_name"])); 

	$profsql =  "SELECT * FROM tblaccount Where username = '$profuser'";
	$profresult = mysqli_query($conn,$profsql);
	$profresultCheck = mysqli_num_rows($profresult);
	
	$profsql1 =  "SELECT * FROM tblprofessor Where facultyID = '$profNo'";
	$profresult1 = mysqli_query($conn,$profsql1);
	$profresultCheck1 = mysqli_num_rows($profresult1);
	
	$profsql2 =  "SELECT * FROM tbluser Where email = '$profemail'";
	$profresult2 = mysqli_query($conn,$profsql2);
	$profresultCheck2 = mysqli_num_rows($profresult2);
	
	$profsql3 = "select password from tblaccount";
	$profresult3 = mysqli_query($conn,$profsql3);
	$var = "";
	while($profresultCheck3 = mysqli_fetch_assoc($profresult3)){
	$hashedPwdCheck = password_verify($profpass, $profresultCheck3['password']);
	if($hashedPwdCheck == false){
		$var = "false";
	}else if ($hashedPwdCheck == true){
		$var = "true";
		break;
	}
	}
	
	
	
	if ($profresultCheck > 0)
	{
		echo "<script type ='text/javascript'>alert('Username already exist');
	window.location.href='../home.php';
	</script>";
	}
	elseif($profresultCheck1 > 0){
		echo "<script type ='text/javascript'>alert('Faculty ID already exist');
	window.location.href='../home.php';
	</script>";
	}
	elseif($profresultCheck2 > 0){
		echo "<script type ='text/javascript'>alert('E-mail already exist');
	window.location.href='../home.php';
	</script>";
	}
	elseif($var == ""){
		echo "<script type ='text/javascript'>window.location.href='../home.php';
	</script>";
	}elseif($var == "true"){
		echo "<script type ='text/javascript'>alert('Password already exist');
	window.location.href='../home.php';
	</script>";
	}
	elseif($var == "false") {
		
		$validate = "select * from tblcor where facultyID = '".$profNo."'";
		$valid = mysqli_query($conn,$validate);
		$check = mysqli_fetch_assoc($valid);
		
		$check1 = "select * from tbldeanandvice where facultyID = '".$profNo."'";
			$res = mysqli_query($conn,$check1);
			$row = mysqli_fetch_assoc($res);
		
		if($check > 1){
			$chair = "select * from tblprogramchairs where facultyID = '".$profNo."'";
			$setchair = mysqli_query($conn,$chair);
			$getchair = mysqli_fetch_assoc($setchair);
			
			if($getchair > 1){
				$profhashedPwd = password_hash($profpass, PASSWORD_DEFAULT);
		
		$profsql = "INSERT INTO tbluser (role, firstname, lastname, gender, email)
		VALUES ('dept.chair','".$_SESSION['post']."".$proffirst."', '$proflast', '$profgend', '$profemail');
		
		INSERT INTO tblaccount (userID, username, password)
		VALUES ('$userID','$profuser', '$profhashedPwd');";
		
		mysqli_multi_query($conn, $profsql);
		
		$_SESSION['type'] = "dept.chair";
		
		$_SESSION['profid'] = $profNo;
		$_SESSION['profpic'] = $file;
		
		$_SESSION['proffirstname'] = $proffirst;
		$_SESSION['proflastname'] = $proflast;
		$_SESSION['profgen'] = $profgend;
		$_SESSION['profmail'] = $profemail;
		
		$_SESSION['profuser'] = $profuser;
		$_SESSION['profpwd'] = $profhashedPwd;
		
		header("Location: tblprof.php");
			exit();
			}
			elseif($row > 1){
				if($row['position'] == "dean"){
					$profhashedPwd = password_hash($profpass, PASSWORD_DEFAULT);
		
		$profsql = "INSERT INTO tbluser (role, firstname, lastname, gender, email)
		VALUES ('dean','".$_SESSION['post']."".$proffirst."', '$proflast', '$profgend', '$profemail');
		
		INSERT INTO tblaccount (userID, username, password)
		VALUES ('$userID','$profuser', '$profhashedPwd');";
		
		mysqli_multi_query($conn, $profsql);
		
		$_SESSION['type'] = "dean";
		
		$_SESSION['profid'] = $profNo;
		$_SESSION['profpic'] = $file;
		
		$_SESSION['proffirstname'] = $proffirst;
		$_SESSION['proflastname'] = $proflast;
		$_SESSION['profgen'] = $profgend;
		$_SESSION['profmail'] = $profemail;
		
		$_SESSION['profuser'] = $profuser;
		$_SESSION['profpwd'] = $profhashedPwd;
		
		header("Location: tblprof.php");
			exit();
				}elseif($row['position'] == "vicedean"){
					$profhashedPwd = password_hash($profpass, PASSWORD_DEFAULT);
		
		$profsql = "INSERT INTO tbluser (role, firstname, lastname, gender, email)
		VALUES ('vicedean','".$_SESSION['post']."".$proffirst."', '$proflast', '$profgend', '$profemail');
		
		INSERT INTO tblaccount (userID, username, password)
		VALUES ('$userID','$profuser', '$profhashedPwd');";
		
		mysqli_multi_query($conn, $profsql);
		
		$_SESSION['type'] = "vicedean";
		
		$_SESSION['profid'] = $profNo;
		$_SESSION['profpic'] = $file;
		
		$_SESSION['proffirstname'] = $proffirst;
		$_SESSION['proflastname'] = $proflast;
		$_SESSION['profgen'] = $profgend;
		$_SESSION['profmail'] = $profemail;
		
		$_SESSION['profuser'] = $profuser;
		$_SESSION['profpwd'] = $profhashedPwd;
		
		header("Location: tblprof.php");
			exit();
				}
			}else{
				$profhashedPwd = password_hash($profpass, PASSWORD_DEFAULT);
		
		$profsql = "INSERT INTO tbluser (role, firstname, lastname, gender, email)
		VALUES ('professor','".$_SESSION['post']."".$proffirst."', '$proflast', '$profgend', '$profemail');
		
		INSERT INTO tblaccount (userID, username, password)
		VALUES ('$userID','$profuser', '$profhashedPwd');";
		
		mysqli_multi_query($conn, $profsql);
		
		$_SESSION['type'] = "prof";
		
		$_SESSION['profid'] = $profNo;
		$_SESSION['profpic'] = $file;
		
		$_SESSION['proffirstname'] = $proffirst;
		$_SESSION['proflastname'] = $proflast;
		$_SESSION['profgen'] = $profgend;
		$_SESSION['profmail'] = $profemail;
		
		$_SESSION['profuser'] = $profuser;
		$_SESSION['profpwd'] = $profhashedPwd;
		header("Location: tblprof.php");
			exit();
			}
			
		}else{
		echo "<script type ='text/javascript'>alert('No schedule exist. Please contact the Administrator');
	window.location.href='../home.php';
	</script>";
		}
}
}
?>