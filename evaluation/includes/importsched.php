<?php
session_start();
include_once 'db.inc.php';

if(isset($_POST['btnSched'])){
	if(!empty($_FILES["excelfile"]["tmp_name"])){
		$fileName = explode(".",$_FILES["excelfile"]["name"]);
		if($fileName[1]=="csv"){
			
			$del = "delete from tblcor;
			";
			mysqli_multi_query($conn,$del);
			
			$file = $_FILES["excelfile"]["tmp_name"];
			$openFile = fopen($file,"r");
			
			$number = 0;
			while($dataFile = fgetcsv($openFile, 3000,",")){
				$number++;
				$yr = $dataFile[0];
				$id = $dataFile[1];
				$subj = $dataFile[2];
				$day = $dataFile[3];
				$time = $dataFile[4];
				$room = $dataFile[5];
				
				if($number != 1){
					$sql = "insert into tblcor(yr,facultyID,subjectCode,day,time,room) values ('$yr','$id','$subj','$day','$time','$room')";
					mysqli_query($conn,$sql);
				}
			}
			
			header("Location: ../admin.php?processing...");
			exit();
		}else{
			header("Location: ../admin.php?you_must_choose_a_csv_file");
			exit();
		}
	}else{
			header("Location: ../admin.php?you_must_choose_a_file");
			exit();
		}
}

?>