$(document).ready(function() {
	$("#subj").change(function() { 
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"get-room.php",
				data:{room:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#room").html(resp);
				}
			});
			
		} else {
			$("#room").html("<span>Please choose...</span>");
		}
		
	});
});
