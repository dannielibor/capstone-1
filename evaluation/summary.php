<?php
session_start();
include_once'includes/db.inc.php';
?>
<!DOCTYPE html>
<html>

	<head>
	 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	 <script type="text/javascript" src="https://www.google.com/jsapi"></script>
	 <script type="text/javascript" src="jquery.min.js"></script>
	 <script type="text/javascript" src="sum-year.js"></script>
	 
	 <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = new google.visualization.DataTable();

data.addColumn('string', 'a');
data.addColumn('number', '<?php
$sql = "select * from tblprofessor where facultyID = '".$_SESSION['facid']."'";
$res = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($res);
$name = "select * from tbluser where userID = '".$row['userID']."'";
$set = mysqli_query($conn,$name);
$get = mysqli_fetch_assoc($set);
echo $get['firstname']." ".$get['lastname'];
?>');

<?php
$query = "select * from tblcor where facultyID = '".$_SESSION['facid']."' group by schlyr";
$resquery = mysqli_query($conn,$query);
while($rowquery = mysqli_fetch_assoc($resquery)){
	$query1 = "select * from tblcor where facultyID = '".$_SESSION['facid']."' and schlyr = '".$rowquery['schlyr']."' group by sem";
	$resquery1 = mysqli_query($conn,$query1);
	while($rowquery1 = mysqli_fetch_assoc($resquery1)){
		$query2 = "select * from tblsummary where facultyID = '".$_SESSION['facid']."' and schlyr = '".$rowquery1['schlyr']."' and sem = '".$rowquery1['sem']."'";
		$resquery2 = mysqli_query($conn,$query2);
		$rowquery2 = mysqli_fetch_assoc($resquery2);
		if($rowquery2 < 1){
		}else{
		echo "data.addRows([
		['".$rowquery2['schlyr']." | ".$rowquery2['sem']."',".$rowquery2['total']."]
		])";
		}
	}
}
?>



        var options = {
          title: 'Performance Appraisal',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
   
		<link rel="stylesheet" type="text/css" href="style.css">
		<title>SBCA - Evaluation Form</title>
		<style>
		.ul1 {
			font-family:arial;
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

.li1 {
    float: left;
}

.li1 a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

.li1 a:hover {
    background-color: #111;
}
</style>
	</head>
	
		<body>
		
			<div id="main">
			<div style = "background-color:#DCDCDC;position:absolute;width:100%;height:80%;top:14%"></div>
			<div style = "background-color:#DCDCDC;position:absolute;width:100%;height:50%;top:150%"></div>
		<div id="piechart_3d1" style="top:20%;width: 900px; height: 500px;position:absolute;"></div>
		
		<div id="curve_chart" style="margin-top:15%;margin-left:55%;width: 500px; height: 350px;position:absolute"></div>
		
		<div id="piechart_3d2" style="left:55%;top:85%;width: 600px; height: 500px;position:absolute;"></div>
		
		<div id="columnchart_material1" style="left:10%;top:95%;width: 500px; height: 250px;position:absolute"></div>
		
		<div id="piechart_3d3" style="right:55%;top:145%;width: 600px; height: 500px;position:absolute;"></div>
		
		<div id="columnchart_material2" style="left:55%;top:155%;width: 500px; height: 250px;position:absolute;"></div>
		
		<div id="piechart_3d4" style="left:55%;top:200%;width: 600px; height: 500px;position:absolute;"></div>
		
		<div id="columnchart_material3" style="right:55%;top:210%;width: 500px; height: 250px;position:absolute;"></div>

		<select style = "position:absolute;margin-top:10%;margin-left:60%" name = "year" id="year">
		<option value = "" disabled selected>Please choose...</option>
		<?php
		$sql = "select * from tblcor where facultyID = '".$_SESSION['facid']."' group by schlyr asc";
		$res = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($res)){
		echo "<option>".$row['schlyr']."</option>";
		}
		?>
		</select>
		
		<select name = "sem" id = "sem" style = "position:absolute;margin-top:10%;margin-left:70%">
		<option value = "" disabled selected>Please choose...</option>
		</select>
		
		
		
			<header>
			
				<div class = "container">
				
					<div class = "asd">
					
						<div id="mySidenav" class="sidenav">
						
							<img src="background.jpg" height = "100" width = "100" class = "picture" style="cursor:pointer">
							<a href = "dean.php">Home</a>
							<a href = "profile.php">Profile</a>
							<a href = "appraisalDean.php">FPA</a>
							<a href = "stats.php">Statistics</a>
							<a href = "settings.php">Settings</a>
							<a onclick = "document.getElementById('id02').style.display='block'" style = "cursor:pointer">Logout</a>
						
						</div>
						
						<img src="btnNav.jpg" alt="Navigation" height = "85dp" class = "menu" onclick="openNav()" style="cursor:pointer">
		
						<a href = "account.php"><img src="logo.png" alt="San Beda College Alabang" height = "70dp" class = "logo" style="cursor:pointer"></a>
					
						<font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">San Beda College Alabang</font>

						
						
					</div>
					
					
						<nav class = "nav">
							<ul class = "ul">
								<li class = "li"><a class = "a" href = "#"><?php echo $_SESSION['firstname'] ?></a></li>
							</ul>
						</nav>
						
					
					
				</div>
				
			</div>
			
			</header>
	
			<footer class = "footerhome" style = "top:50%">
			
				<div class = "follow">
				
					<p class = "bottom">FOLLOW US</p>
					
					<ul class = "link">
					
						<li><a href="https://www.google.com.ph/?gfe_rd=cr&dcr=0&ei=12WNWqO6G9GiX_j3gqgJ" target="_blank">
							<image src = "google.png" height = "10"> Google+</a></li><br>
						
						<li><a href="https://twitter.com/sbcasec?lang=en" target="_blank">
							<image src = "twitter.png" height = "10"> Twitter</a></li><br>
							
						<li><a href="https://www.facebook.com/BedaAlabang/" target="_blank">
							<image src = "facebook.png" height = "10"> Facebook</a></li><br>
							
						<li><a href="https://www.instagram.com/?hl=en" target="_blank">
							<image src = "instagram.png" height = "10"> Instagram</a></li><br>
							
						<li><a href="https://www.youtube.com" target="_blank"><image src = "youtube.png" height = "10"> Youtube</a></li>
					
					</ul>
					
				</div>
				
				<div class = "contactUs">
				
					<p class = "bottom">CONTACT US</p>
					
					<ul class = "link">
					
						<li><a class = "aaaa" href="http://www.sanbeda-alabang.edu.ph/bede/" target="_blank">
							Website: http://www.sanbeda-alabang.edu.ph</a></li><br>
							
						<li><a class = "aaaa" href="#home" style = "cursor:auto">E-mail: sbca@sanbeda-alabang.edu.ph</a></li><br>
						
						<li><a class = "aaaa" href="#home" style = "cursor:auto">Telephone: 236-7222</a></li><br>
					
					</ul>
					
				</div>
				
				<div class = "copyright">
					<p id = "copyright"/>
				</div>
	
			</footer>
			
	<script>
		var d = new Date().getFullYear();
		document.getElementById("copyright").innerHTML = "Copyright &copy; " + d + " San Beda College Alabang, All Rights Reserved.";
	</script>
	
			<div id="id02" class="logout">
  
				<form class="logout-content animate" method = "post" action = "includes/logout.php">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('id02').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
						<label for="username"><b class = "align">Are you sure?</b></label><br>
						<button class="button1" name = "logout"><span>Logout </span></button>
					</div>
			</div>

	<script>
	
		var logout = document.getElementById('id02');
		window.onclick = function(event) {
			if (event.target == logout) {
				logout.style.display = "none";
			}
		}
		
		var x = true;
		function openNav() {
			if(x == true) {
				document.getElementById("mySidenav").style.width = "197px";
				document.getElementById("main").style.marginLeft = "197px";
				x = false;
			}
			else {
				closeNav();
				x = true;
			}
		}

		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
			document.getElementById("main").style.marginLeft= "0";
			document.body.style.backgroundColor = "white";
		}
		
	</script>

		</body>
		
</html>