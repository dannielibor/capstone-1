<?php
session_start();
include_once 'includes/db.inc.php';
?>
<!DOCTYPE html>
<html>

	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<title>SBCA - Evaluation Form</title>
	</head>
	
		<body>
		
			<video autoplay muted loop id="myVideo">
			<source src="sbca.mp4" type="video/mp4">
			Your browser does not support HTML5 video.
			</video>
			
			<div id="main">
			
			<div class = "eval" id = "stud" style = "width:60%">
			
			<form action = "includes/import.php" method = "post" enctype = "multipart/form-data">
			
			<input type = "file" name = "excelfile" />
			<input type = "submit" name = "btnImport" value = "Import">
			
			<?php 
			$select = "select * from tblcor order by studentID asc";
			$result = mysqli_query($conn,$select);
			while($row = mysqli_fetch_assoc($result)){
			$sem = $row['sem'];
			$schlyr = $row['schlyr'];
			$course = $row['course'];
			$studID = $row['studentID'];
			$facid = $row['facultyID'];
			$dept = $row['department'];
			$subj = $row['subjectCode'];
			$title = $row['courseTitle'];
			$yr = $row['yrsec'];
			$day = $row['day'];
			$time = $row['time'];
			$room = $row['room'];
			$size = $row['size'];
			?>
			
			<table>
				<tr>
				<th>sem</th>
				<th>schlyr</th>
				<th>course</th>
					<th>studentID</th>
					<th>facultyID</th>
					<th>department</th>
					<th>subject/code</th>
					<th>courseTitle</th>
					<th>year/section</th>
					<th>day</th>
					<th>time</th>
					<th>room</th>
					<th>size</th>
				</tr>
				
				<tr>
					<td><?php echo $sem;?></td>
					<td><?php echo $schlyr;?></td>
					<td><?php echo $course;?></td>
					<td><?php echo $studentID;?></td>
					<td><?php echo $facultyID;?></td>
					<td><?php echo $department;?></td>
					<td><?php echo $subjectCode;?></td>
					<td><?php echo $title;?></td>
					<td><?php echo $yrsec;?></td>
					<td><?php echo $day;?></td>
					<td><?php echo $time;?></td>
					<td><?php echo $room;?></td>
					<td><?php echo $size;?></td>
				</tr>
			</table>
			
			<?php } ?>
			
			</form>
			
			</div>
			
			
			<header>
			
				<div class = "container">
				
					<div class = "asd">
					
						<div id="mySidenav" class="sidenav">
						
							<img src="background.jpg" alt="<?php echo $username; ?>" height = "100" width = "100" class = "picture" style="cursor:pointer">
							<a href = "account.php">Home</a>
							<a href = "profile.php">Accounts</a>
							<a href = "settingsadmin.php">Settings</a>
							<a onclick = "document.getElementById('id02').style.display='block'" style = "cursor:pointer">Logout</a>
						
						</div>
						
						<img src="btnNav.jpg" alt="Navigation" height = "85dp" class = "menu" onclick="openNav()" style="cursor:pointer">
		
						<a href = "account.php"><img src="logo.png" alt="San Beda College Alabang" height = "70dp" class = "logo" style="cursor:pointer"></a>
					
						<font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">San Beda College Alabang</font>

					</div>
					
						<nav class = "nav">
							<ul class = "ul">
								<li class = "li"><a class = "a" href = "#"><?php echo $_SESSION['firstname']; ?></a></li>
							</ul>
						</nav>
						
						<nav class = "tabs">
							<ul class = "tabs_ul" style = "margin-right: 50%">
								<li class = "tabs_li"><a class = "tabs_a"  href = "#" id = "student" value = "student">Import COR</a></li>
							</ul>
						</nav>
						
						<script>
						var a = document.getElementById('student');
						var b = document.getElementById('schedule');
						
						(function() {
        a.onclick = function() { 
		document.getElementById("sched").style.visibility = "hidden";
			document.getElementById("stud").style.visibility = "visible";
        };
    })();
	
	(function() {
        b.onclick = function() { 
		document.getElementById("stud").style.visibility = "hidden";
			document.getElementById("sched").style.visibility = "visible";
        };
    })();
	
						</script>
						
				</div>
				
			</div>
			
			</header>
	
			<footer class = "footerhome">
			
				<div class = "follow">
				
					<p class = "bottom">FOLLOW US</p>
					
					<ul class = "link">
					
						<li><a href="https://www.google.com.ph/?gfe_rd=cr&dcr=0&ei=12WNWqO6G9GiX_j3gqgJ" target="_blank">
							<image src = "google.png" height = "10"> Google+</a></li><br>
						
						<li><a href="https://twitter.com/sbcasec?lang=en" target="_blank">
							<image src = "twitter.png" height = "10"> Twitter</a></li><br>
							
						<li><a href="https://www.facebook.com/BedaAlabang/" target="_blank">
							<image src = "facebook.png" height = "10"> Facebook</a></li><br>
							
						<li><a href="https://www.instagram.com/?hl=en" target="_blank">
							<image src = "instagram.png" height = "10"> Instagram</a></li><br>
							
						<li><a href="https://www.youtube.com" target="_blank"><image src = "youtube.png" height = "10"> Youtube</a></li>
					
					</ul>
					
				</div>
				
				<div class = "contactUs">
				
					<p class = "bottom">CONTACT US</p>
					
					<ul class = "link">
					
						<li><a class = "aaaa" href="http://www.sanbeda-alabang.edu.ph/bede/" target="_blank">
							Website: http://www.sanbeda-alabang.edu.ph</a></li><br>
							
						<li><a class = "aaaa" href="#home" style = "cursor:auto">E-mail: sbca@sanbeda-alabang.edu.ph</a></li><br>
						
						<li><a class = "aaaa" href="#home" style = "cursor:auto">Telephone: 236-7222</a></li><br>
					
					</ul>
					
				</div>
				
				<div class = "copyright">
					<p id = "copyright"/>
				</div>
	
			</footer>
			
	<script>
		var d = new Date().getFullYear();
		document.getElementById("copyright").innerHTML = "Copyright &copy; " + d + " San Beda College Alabang, All Rights Reserved.";
	</script>
	
			<div id="id02" class="logout">
  
				<form class="logout-content animate" method = "post" action = "home.php">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('id02').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
						<label for="username"><b class = "align">Are you sure?</b></label><br>
						<button class="button1" ><span>Logout </span></button>
					</div>
			</div>

	<script>
		var view = document.getElementById('view');
		var edit = document.getElementById('edit');
		var logout = document.getElementById('id02');
		window.onclick = function(event) {
			if (event.target == logout) {
				logout.style.display = "none";
			}
			if (event.target == view) {
				view.style.display = "none";
				}
			if (event.target == edit) {
				edit.style.display = "none";
				}	
		}
		
		var x = true;
		function openNav() {
			if(x == true) {
				document.getElementById("mySidenav").style.width = "197px";
				document.getElementById("main").style.marginLeft = "197px";
				x = false;
			}
			else {
				closeNav();
				x = true;
			}
		}

		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
			document.getElementById("main").style.marginLeft= "0";
			document.body.style.backgroundColor = "white";
		}
		
	</script>

		</body>
		
</html>