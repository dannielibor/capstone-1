<?php
session_start();
include_once 'includes/db.inc.php';
try{
								if(!empty($_GET['yr'])){
								$_SESSION['schoolYear'] =  $_GET['yr'];
								}else{
								throw new Exception('<script>
					var date = new Date().getFullYear();	
					date = date-1+"-"+date;
					window.location.href = "student.php?yr="+date;
					</script>');
								}
							}catch(Exception $e){
								
							}
?>
<!DOCTYPE html>
<html>

	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<title>SBCA - Evaluation Form</title>
		<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="js.js"></script>


	</head>
	
		<body>
		
			<div id="main">
			
			<div id = "a">
			
				<div class = "eval" style = "height:330px;">

					<form method = "post">
						<fieldset>
						<legend>STUDENT'S EVALUATION OF FACULTY</legend>
						
							<label for = "prof"><b>Professor: </b></label>
							<select class = "list"	name = "prof" id="prof">
								<option value = "" disabled selected>Please choose...</option>
							
							<?php 
							
							$query = "select * from tblcor where studentID = '".$_SESSION['id']."' and sem = '".$_SESSION['sem']."' and schlyr = '".$_SESSION['schoolYear']."' group by facultyID";
							$res = mysqli_query($conn,$query);
							
							while($row = mysqli_fetch_assoc($res)){
								
								$fac = $row['facultyID'];
								$cor = $row['regID'];
								
								$sel = "select * from tblprofessor where facultyID = '".$fac."'";
								$result = mysqli_query($conn,$sel);
								$row2 = mysqli_fetch_assoc($result);
								
								$userid = $row2['userID'];
								
								$name = "select * from tbluser where userID = '".$userid."'";
								$resname = mysqli_query($conn,$name);
								$data = mysqli_fetch_assoc($resname);
								
								echo "<option value = '".$cor."'>".$data['firstname']." ".$data['lastname']."</option>";
							}	
			?>
							</select>
							
						<br><br>
						
							<label for = "subj"><b>Subject/Code: </b></label>
							<select name="subj" id="subj">
							<option value = "" disabled selected>Please choose...</option>
							
							</select>
							
						<br><br>
						
							<label for = "dep"><b>Department: </b><span name = "dep" id = "dep"></label>
							
						<br><br>
						
							<label for = "schlYr"><b>School Year: </b><span id = "schlYr"></label>
							
							<label for = "sem"><b>Semester: </b></label>
							<?php
							
	echo "<span>".$_SESSION['sem']."</span>";
							?>
							
						</fieldset>
						
						<br><br>
						
						<fieldset>
						<legend>CLASS SCHEDULE</legend>
						
							<label for = "day"><b>Day: </b><span name = "day" id = "day"></label>
								
						<br><br>
							
							<label for = "time"><b>Time: </b><span name = "time" id = "time"></label>
								
							<br><br>
							
							<label for = "room"><b>Room: </b><span name = "room" id = "room"></label>
							
						</fieldset>
						
					</form>

				</div>
				<form method = "post" action = "includes/eval.php">
				<div class = "eval" style = "margin-top:38%;text-align:left;height:90%">
				<table>
					<tr>
						<th style = "color:#000000">A.</th>
						<th style = "text-align:left;color:#000000">METHODS AND STRATEGIES OF TEACHING (40%)</th>
						<th style = "color:#000000">5</th>
						<th style = "color:#000000">4</th>
						<th style = "color:#000000">3</th>
						<th style = "color:#000000">2</th>
						<th style = "color:#000000">1</th>
					</tr>
					
					<tr>
						<td>1.</td>
						<td style = "color: #800000;">Starts the lesson in an interesting manner.</td>
						<td><input type = "radio" name = "qa1" value = "5"></td>
						<td><input type = "radio" name = "qa1" value = "4"></td>
						<td><input type = "radio" name = "qa1" value = "3"></td>
						<td><input type = "radio" name = "qa1" value = "2"></td>
						<td><input type = "radio" name = "qa1" value = "1"></td>
						
					</tr>
					<tr>
						<td>2.</td>
						<td class = "highlight">Clarifies the objective/s of the lesson.</td>
						<td class = "highlight"><input type = "radio" name = "qa2" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qa2" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qa2" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qa2" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qa2" value = "1"></td>
					</tr>
					<tr>
						<td>3.</td>
						<td style = "color: #800000;">Makes the lesson practical and relevant by relating it with everyday living.</td>
						<td><input type = "radio" name = "qa3" value = "5"></td>
						<td><input type = "radio" name = "qa3" value = "4"></td>
						<td><input type = "radio" name = "qa3" value = "3"></td>
						<td><input type = "radio" name = "qa3" value = "2"></td>
						<td><input type = "radio" name = "qa3" value = "1"></td>
					</tr>
					<tr>
						<td>4.</td>
						<td class = "highlight">Makes the lesson practical and relevant by relating it with everyday living.</td>
						<td class = "highlight"><input type = "radio" name = "qa4" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qa4" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qa4" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qa4" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qa4" value = "1"></td>
					</tr>
					<tr>
						<td>5.</td>
						<td style = "color: #800000;">Facilitates student's understanding by rephrasing questions using synonyms, giving examples and the like.</td>
						<td><input type = "radio" name = "qa5" value = "5"></td>
						<td><input type = "radio" name = "qa5" value = "4"></td>
						<td><input type = "radio" name = "qa5" value = "3"></td>
						<td><input type = "radio" name = "qa5" value = "2"></td>
						<td><input type = "radio" name = "qa5" value = "1"></td>
					</tr>
					<tr>
						<td>6.</td>
						<td class = "highlight">Formulates questions that challenge the student's imagination or reasoning ability.</td>
						<td class = "highlight"><input type = "radio" name = "qa6" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qa6" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qa6" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qa6" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qa6" value = "1"></td>
					</tr>
					<tr>
						<td>7.</td>
						<td style = "color: #800000;">Gives student's learning tasks; exercises related to the topics learned/being learned.</td>
						<td><input type = "radio" name = "qa7" value = "5"></td>
						<td><input type = "radio" name = "qa7" value = "4"></td>
						<td><input type = "radio" name = "qa7" value = "3"></td>
						<td><input type = "radio" name = "qa7" value = "2"></td>
						<td><input type = "radio" name = "qa7" value = "1"></td>
					</tr>
					<tr>
						<td>8.</td>
						<td class = "highlight">Uses instructional time productively by properly apportioning it to all class activities.</td>
						<td class = "highlight"><input type = "radio" name = "qa8" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qa8" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qa8" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qa8" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qa8" value = "1"></td>
					</tr>
					<tr>
						<td>9.</td>
						<td style = "color: #800000;">Encourages maximum participation of the students in all activites.</td>
						<td><input type = "radio" name = "qa9" value = "5"></td>
						<td><input type = "radio" name = "qa9" value = "4"></td>
						<td><input type = "radio" name = "qa9" value = "3"></td>
						<td><input type = "radio" name = "qa9" value = "2"></td>
						<td><input type = "radio" name = "qa9" value = "1"></td>
					</tr>
					<tr>
						<td>10.</td>
						<td class = "highlight">Makes use of the whiteboard and other audiovisual materials like pictures, graphs, and outlines to make the lesson more interesting.</td>
						<td class = "highlight"><input type = "radio" name = "qa10" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qa10" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qa10" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qa10" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qa10" value = "1"></td>
					</tr>
				</table>
				</div>
				</div>
				<!--page 2!-->
				<div id = "b" style = "visibility:hidden;">
				<div class = "eval" style = "height: 48%;text-align:left">
				<table>
				<tr>
						<td>11.</td>
						<td style = "color: #800000;">Provides students with more opportunities for learning by giving assignments, research work, and the like which could be accomplished.</td>
						<td><input type = "radio" name = "qa11" value = "5"></td>
						<td><input type = "radio" name = "qa11" value = "4"></td>
						<td><input type = "radio" name = "qa11" value = "3"></td>
						<td><input type = "radio" name = "qa11" value = "2"></td>
						<td><input type = "radio" name = "qa11" value = "1"></td>
					</tr>
					<tr>
						<td>12.</td>
						<td class = "highlight">Encourages students to ask intelligent and relevant questions.</td>
						<td class = "highlight"><input type = "radio" name = "qa12" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qa12" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qa12" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qa12" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qa12" value = "1"></td>
					</tr>
					<tr>
						<td>13.</td>
						<td style = "color: #800000;">Establishes continuity and unity of present and previous lessons. Summarizes the lesson to bring out important points</td>
						<td><input type = "radio" name = "qa13" value = "5"></td>
						<td><input type = "radio" name = "qa13" value = "4"></td>
						<td><input type = "radio" name = "qa13" value = "3"></td>
						<td><input type = "radio" name = "qa13" value = "2"></td>
						<td><input type = "radio" name = "qa13" value = "1"></td>
					</tr>
					<tr>
						<td>14.</td>
						<td class = "highlight">Evaluates students performance by giving quizzes from time to time.</td>
						<td class = "highlight"><input type = "radio" name = "qa14" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qa14" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qa14" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qa14" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qa14" value = "1"></td>
					</tr>
					<tr>
						<td>15.</td>
						<td style = "color: #800000;">Informs students of test results not later than two weeks after the exam.</td>
						<td><input type = "radio" name = "qa15" value = "5"></td>
						<td><input type = "radio" name = "qa15" value = "4"></td>
						<td><input type = "radio" name = "qa15" value = "3"></td>
						<td><input type = "radio" name = "qa15" value = "2"></td>
						<td><input type = "radio" name = "qa15" value = "1"></td>
					</tr>
					<tr>
						<td>16.</td>
						<td class = "highlight">Explains the grading system and gives grades fairly.</td>
						<td class = "highlight"><input type = "radio" name = "qa16" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qa16" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qa16" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qa16" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qa16" value = "1"></td>
					</tr>
					</table>
				</div>
				<div class = "eval" style = "margin-top:36%;height:25%;text-align:left">
				<table>
					<tr>
						<th style = "color:#000000">B.</th>
						<th style = "text-align:left;color:#000000">MASTERY OF THE SUBJECT MATTER (20%)</th>
						<th style = "color:#000000">5</th>
						<th style = "color:#000000">4</th>
						<th style = "color:#000000">3</th>
						<th style = "color:#000000">2</th>
						<th style = "color:#000000">1</th>
					</tr>
					
					<tr>
						<td>1.</td>
						<td style = "color: #800000;">Presents the lessons systematically.</td>
						<td><input type = "radio" name = "qb1" value = "5"></td>
						<td><input type = "radio" name = "qb1" value = "4"></td>
						<td><input type = "radio" name = "qb1" value = "3"></td>
						<td><input type = "radio" name = "qb1" value = "2"></td>
						<td><input type = "radio" name = "qb1" value = "1"></td>
					</tr>
					<tr>
						<td>2.</td>
						<td class = "highlight">Explains the lesson/objectives clearly.</td>
						<td class = "highlight"><input type = "radio" name = "qb2" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qb2" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qb2" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qb2" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qb2" value = "1"></td>
					</tr>
					<tr>
						<td>3.</td>
						<td style = "color: #800000;">Answers question of the students intelligently and satisfactorily.</td>
						<td><input type = "radio" name = "qb3" value = "5"></td>
						<td><input type = "radio" name = "qb3" value = "4"></td>
						<td><input type = "radio" name = "qb3" value = "3"></td>
						<td><input type = "radio" name = "qb3" value = "2"></td>
						<td><input type = "radio" name = "qb3" value = "1"></td>
					</tr>
				</table>
				</div>
				
				<div class = "eval" style = "margin-top:53%;height:20%;text-align:left">
				<table>
					<tr>
						<th style = "color:#000000">C.</th>
						<th style = "text-align:left;color:#000000">COMMUNICATION SKILLS (15%)</th>
						<th style = "color:#000000">5</th>
						<th style = "color:#000000">4</th>
						<th style = "color:#000000">3</th>
						<th style = "color:#000000">2</th>
						<th style = "color:#000000">1</th>
					</tr>
					
					<tr>
						<td>1.</td>
						<td style = "color: #800000;">Speaks correct English and/or Filipino and communicates his/her ideas well.</td>
						<td><input type = "radio" name = "qc1" value = "5"></td>
						<td><input type = "radio" name = "qc1" value = "4"></td>
						<td><input type = "radio" name = "qc1" value = "3"></td>
						<td><input type = "radio" name = "qc1" value = "2"></td>
						<td><input type = "radio" name = "qc1" value = "1"></td>
					</tr>
					<tr>
						<td>2.</td>
						<td class = "highlight">Has a loud voice enough to be heard by everyone in class.</td>
						<td class = "highlight"><input type = "radio" name = "qc2" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qc2" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qc2" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qc2" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qc2" value = "1"></td>
					</tr>
				</table>
				</div>
				
				<div class = "eval" style = "margin-top:67%;height:50%;text-align:left">
				<table>
					<tr>
						<th style = "color:#000000">D.</th>
						<th style = "text-align:left;color:#000000">CLASSROOM MANAGEMENT (15%)</th>
						<th style = "color:#000000">5</th>
						<th style = "color:#000000">4</th>
						<th style = "color:#000000">3</th>
						<th style = "color:#000000">2</th>
						<th style = "color:#000000">1</th>
					</tr>
					
					<tr>
						<td>1.</td>
						<td style = "color: #800000;">Sees to it that the room is clean and orderly at all times.</td>
						<td><input type = "radio" name = "qd1" value = "5"></td>
						<td><input type = "radio" name = "qd1" value = "4"></td>
						<td><input type = "radio" name = "qd1" value = "3"></td>
						<td><input type = "radio" name = "qd1" value = "2"></td>
						<td><input type = "radio" name = "qd1" value = "1"></td>
					</tr>
					<tr>
						<td>2.</td>
						<td class = "highlight">Maintains classroom discipline.</td>
						<td class = "highlight"><input type = "radio" name = "qd2" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qd2" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qd2" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qd2" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qd2" value = "1"></td>
					</tr>
					<tr>
						<td>3.</td>
						<td style = "color: #800000;">Checks the attendance of students.</td>
						<td><input type = "radio" name = "qd3" value = "5"></td>
						<td><input type = "radio" name = "qd3" value = "4"></td>
						<td><input type = "radio" name = "qd3" value = "3"></td>
						<td><input type = "radio" name = "qd3" value = "2"></td>
						<td><input type = "radio" name = "qd3" value = "1"></td>
					</tr>
					<tr>
						<td>4.</td>
						<td class = "highlight">Sees to it that the students wear the prescribed uniform.</td>
						<td class = "highlight"><input type = "radio" name = "qd4" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qd4" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qd4" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qd4" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qd4" value = "1"></td>
					</tr>
					<tr>
						<td>5.</td>
						<td style = "color: #800000;">Sees to it that all teaching materials like whiteboard marker and erasers are prepared.</td>
						<td><input type = "radio" name = "qd5" value = "5"></td>
						<td><input type = "radio" name = "qd5" value = "4"></td>
						<td><input type = "radio" name = "qd5" value = "3"></td>
						<td><input type = "radio" name = "qd5" value = "2"></td>
						<td><input type = "radio" name = "qd5" value = "1"></td>
					</tr>
					<tr>
						<td>6.</td>
						<td class = "highlight">Turns off airconditioning units, all lights, and make sure that the whiteboard is clean before dismissing the class.</td>
						<td class = "highlight"><input type = "radio" name = "qd6" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qd6" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qd6" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qd6" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qd6" value = "1"></td>
					</tr>
				</table>
				</div>
				</div>
				<!--page 3!-->
				<div id = "c" style = "visibility:hidden;">
				<div class = "eval" style = "height:70%;text-align:left">
				<table>
					<tr>
						<th style = "color:#000000">E.</th>
						<th style = "text-align:left;color:#000000">PERSONAL AND SOCIAL TRAITS (10%)</th>
						<th style = "color:#000000">5</th>
						<th style = "color:#000000">4</th>
						<th style = "color:#000000">3</th>
						<th style = "color:#000000">2</th>
						<th style = "color:#000000">1</th>
					</tr>
					
					<tr>
						<td>1.</td>
						<td style = "color: #800000;">Projects self-confidence; respects the opinions, suggestions, and comments of students.</td>
						<td><input type = "radio" name = "qe1" value = "5"></td>
						<td><input type = "radio" name = "qe1" value = "4"></td>
						<td><input type = "radio" name = "qe1" value = "3"></td>
						<td><input type = "radio" name = "qe1" value = "2"></td>
						<td><input type = "radio" name = "qe1" value = "1"></td>
					</tr>
					<tr>
						<td>2.</td>
						<td class = "highlight">Projects a respectable and a decent image.</td>
						<td class = "highlight"><input type = "radio" name = "qe2" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qe2" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qe2" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qe2" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qe2" value = "1"></td>
					</tr>
					<tr>
						<td>3.</td>
						<td style = "color: #800000;">Practices what he/she preaches/advises.</td>
						<td><input type = "radio" name = "qe3" value = "5"></td>
						<td><input type = "radio" name = "qe3" value = "4"></td>
						<td><input type = "radio" name = "qe3" value = "3"></td>
						<td><input type = "radio" name = "qe3" value = "2"></td>
						<td><input type = "radio" name = "qe3" value = "1"></td>
					</tr>
					<tr>
						<td>4.</td>
						<td class = "highlight">Manifests and promotes the Benedictine charism of work and prayer.</td>
						<td class = "highlight"><input type = "radio" name = "qe4" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qe4" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qe4" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qe4" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qe4" value = "1"></td>
					</tr>
					<tr>
						<td>5.</td>
						<td style = "color: #800000;">Integrates positive values in the lesson.</td>
						<td><input type = "radio" name = "qe5" value = "5"></td>
						<td><input type = "radio" name = "qe5" value = "4"></td>
						<td><input type = "radio" name = "qe5" value = "3"></td>
						<td><input type = "radio" name = "qe5" value = "2"></td>
						<td><input type = "radio" name = "qe5" value = "1"></td>
					</tr>
					<tr>
						<td>6.</td>
						<td class = "highlight">Recognizes students' participation in institutional activities by giving proper considerations.</td>
						<td class = "highlight"><input type = "radio" name = "qe6" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qe6" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qe6" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qe6" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qe6" value = "1"></td>
					</tr>
					<tr>
						<td>7.</td>
						<td style = "color: #800000;">Comes to class on regularly.</td>
						<td><input type = "radio" name = "qe7" value = "5"></td>
						<td><input type = "radio" name = "qe7" value = "4"></td>
						<td><input type = "radio" name = "qe7" value = "3"></td>
						<td><input type = "radio" name = "qe7" value = "2"></td>
						<td><input type = "radio" name = "qe7" value = "1"></td>
					</tr>
					<tr>
						<td>8.</td>
						<td class = "highlight">Comes to class on time.</td>
						<td class = "highlight"><input type = "radio" name = "qe8" value = "5"></td>
						<td class = "highlight"><input type = "radio" name = "qe8" value = "4"></td>
						<td class = "highlight"><input type = "radio" name = "qe8" value = "3"></td>
						<td class = "highlight"><input type = "radio" name = "qe8" value = "2"></td>
						<td class = "highlight"><input type = "radio" name = "qe8" value = "1"></td>
					</tr>
					<tr>
						<td>9.</td>
						<td style = "color: #800000;">Is approachable and encourages students to consult him/her for assistance.</td>
						<td><input type = "radio" name = "qe9" value = "5"></td>
						<td><input type = "radio" name = "qe9" value = "4"></td>
						<td><input type = "radio" name = "qe9" value = "3"></td>
						<td><input type = "radio" name = "qe9" value = "2"></td>
						<td><input type = "radio" name = "qe9" value = "1"></td>
					</tr>
				</table>
				<div class = "eval" style = "height:50%;top:80%;right:0.3%;text-align:left">
				<textarea rows="16" cols="97" maxlength = "100" name="comment" required pattern="[^'\x22]+" title="Invalid input" 
							placeholder="Enter comment" style = "resize:none"></textarea>
				
				<button class="sign" style = "margin-left:38%;margin-right:50%;width:20%;position:absolute" name = "submit">
				<span>Submit </span></button>
				</div>
				</div>
				</div>
				</form>
			<header>
			
				<div class = "container">
				
					<div class = "asd">
					
						<div id="mySidenav" class="sidenav">
						 <?php  
                $query2 = "SELECT * FROM tblstudent where studentID = '".$_SESSION['id']."'";  
                $result2 = mysqli_query($conn, $query2);  
               if($row2 = mysqli_fetch_array($result2))  
                {  
                     echo '  
                          
                                    <img src="data:image/jpeg;base64,'.base64_encode($row2['picture'] ).'" height="100" width = "100" class = "picture" style = "cursor:pointer" />  
                         
                     ';  
                }else{
				echo '<img src="default.png" height="100" width = "100" class = "picture" style = "cursor:pointer" />';
				}					
                ?>  
						
							
							<a href = "student.php">Home</a>
							<!--<a href = "profile.php">Profile</a>
							<a href = "settings.php">Settings</a>!-->
							<a onclick = "document.getElementById('id02').style.display='block'" style = "cursor:pointer">Logout</a>
						
						</div>
						
						<img src="btnNav.jpg" alt="Navigation" height = "85dp" class = "menu" onclick="openNav()" style="cursor:pointer">
		
						<a href = "student.php"><img src="logo.png" alt="San Beda College Alabang" height = "70dp" class = "logo" style="cursor:pointer"></a>
					
						<font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">San Beda College Alabang</font>

					</div>
					
						<nav class = "nav">
							<ul class = "ul">
								<li class = "li"><a style = "margin-left:-15%" class = "a" href = "#"><?php echo $_SESSION['firstname']." ".$_SESSION['lastname'];?></a></li>
							</ul>
						</nav>
						<div class = "eval" style = "margin-top:10%;position:fixed;height:30%;margin-left:80%;width:15%;text-align:left">
				
			<label>RESPONSE CODE:</label><br><br>
<label>5 = Outstanding</label><br><br>
<label>4 = Very Satisfactory</label><br><br>
<label>3 = Satisfactory</label><br><br>
<label>2 = Moderate Satisfactory</label><br><br>
<label>1 = Needs Improvement</label>			
			
			</div>
				</div>
				
			</div>
			
			<div class="pagination" style = "margin-left:40%;top:200%;position:absolute">
  <a href="#" id = "back">&laquo;</a>
  <a href="#" value = "1" id = "p1">1</a>
  <a href="#" value = "2" id = "p2">2</a>
  <a href="#" value = "3" id = "p3">3</a>
  <a href="#" id = "next">&raquo;</a>
  <script>
  
  var date = new Date().getFullYear();	
		document.getElementById("schlYr").innerHTML = date-1 + " - " + date + " | ";

  
  var p1 = document.getElementById('p1');
  var p2 = document.getElementById('p2');
  var p3 = document.getElementById('p3');
  var back = document.getElementById('back');
  var next = document.getElementById('next');
  var num = 1; 
(function() {
        next.onclick = function() { 
			if(num == 1){
			document.getElementById("b").style.visibility = "visible";
			document.getElementById("c").style.visibility = "hidden";
			document.getElementById("a").style.visibility = "hidden"; num = 2; 
			}else if (num == 2){
			document.getElementById("a").style.visibility = "hidden"; num = 3;
			document.getElementById("b").style.visibility = "hidden";
			document.getElementById("c").style.visibility = "visible";
			}else if (num == 3){
			document.getElementById("a").style.visibility = "visible"; num = 1;
			document.getElementById("b").style.visibility = "hidden";
			document.getElementById("c").style.visibility = "hidden";
			}return num;
        };
    })(); 
	
	(function() {
        back.onclick = function() { 
			if(num == 1){
			document.getElementById("b").style.visibility = "hidden";
			document.getElementById("c").style.visibility = "hidden";
			document.getElementById("a").style.visibility = "visible"; num = 1;
			}else if (num == 2){
			document.getElementById("a").style.visibility = "visible"; num = 1;
			document.getElementById("b").style.visibility = "hidden";
			document.getElementById("c").style.visibility = "hidden";
			}else if (num == 3){
			document.getElementById("a").style.visibility = "hidden"; num = 2;
			document.getElementById("b").style.visibility = "visible";
			document.getElementById("c").style.visibility = "hidden";
			}return num;
        };
    })(); 
  
(function() {
        p1.onclick = function() { 
		document.getElementById("b").style.visibility = "hidden";
		document.getElementById("c").style.visibility = "hidden";
			document.getElementById("a").style.visibility = "visible"; num = 1;
        };
    })();
(function() {
        p2.onclick = function() { 
			document.getElementById("b").style.visibility = "visible";
			document.getElementById("c").style.visibility = "hidden";
			document.getElementById("a").style.visibility = "hidden"; num = 2;
        };
    })();	
	(function() {
        p3.onclick = function() { 
			document.getElementById("b").style.visibility = "hidden";
			document.getElementById("c").style.visibility = "visible";
			document.getElementById("a").style.visibility = "hidden"; num = 3;
        };
    })();
  </script>
</div>
			
			</header>
	
			<footer class = "footerhome">
			
				<div class = "follow">
				
					<p class = "bottom">FOLLOW US</p>
					
					<ul class = "link">
					
						<li><a href="https://www.google.com.ph/?gfe_rd=cr&dcr=0&ei=12WNWqO6G9GiX_j3gqgJ" target="_blank">
							<image src = "google.png" height = "10"> Google+</a></li><br>
						
						<li><a href="https://twitter.com/sbcasec?lang=en" target="_blank">
							<image src = "twitter.png" height = "10"> Twitter</a></li><br>
							
						<li><a href="https://www.facebook.com/BedaAlabang/" target="_blank">
							<image src = "facebook.png" height = "10"> Facebook</a></li><br>
							
						<li><a href="https://www.instagram.com/?hl=en" target="_blank">
							<image src = "instagram.png" height = "10"> Instagram</a></li><br>
							
						<li><a href="https://www.youtube.com" target="_blank"><image src = "youtube.png" height = "10"> Youtube</a></li>
					
					</ul>
					
				</div>
				
				<div class = "contactUs">
				
					<p class = "bottom">CONTACT US</p>
					
					<ul class = "link">
					
						<li><a class = "aaaa" href="http://www.sanbeda-alabang.edu.ph/bede/" target="_blank">
							Website: http://www.sanbeda-alabang.edu.ph</a></li><br>
							
						<li><a class = "aaaa" href="#home" style = "cursor:auto">E-mail: sbca@sanbeda-alabang.edu.ph</a></li><br>
						
						<li><a class = "aaaa" href="#home" style = "cursor:auto">Telephone: 236-7222</a></li><br>
					
					</ul>
					
				</div>
				
				<div class = "copyright">
					<p id = "copyright"/>
				</div>
	
			</footer>
			
	<script>
		var d = new Date().getFullYear();
		document.getElementById("copyright").innerHTML = "Copyright &copy; " + d + " San Beda College Alabang, All Rights Reserved.";
	</script>
	
			<div id="id02" class="logout">
  
				<form class="logout-content animate" method = "post" action = "includes/logout.php">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('id02').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
						<label for="username"><b class = "align">Are you sure?</b></label><br>
						<button class="button1" name = "logout"><span>Logout </span></button>
					</div>
			</div>

	<script>
	
		var logout = document.getElementById('id02');
		window.onclick = function(event) {
			if (event.target == logout) {
				logout.style.display = "none";
			}
		}
		
		var x = true;
		function openNav() {
			if(x == true) {
				document.getElementById("mySidenav").style.width = "197px";
				document.getElementById("main").style.marginLeft = "197px";
				x = false;
			}
			else {
				closeNav();
				x = true;
			}
		}

		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
			document.getElementById("main").style.marginLeft= "0";
			document.body.style.backgroundColor = "white";
		}
		
	</script>

		</body>
		
</html>