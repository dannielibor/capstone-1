$(document).ready(function() {
	$("#subj").change(function() { 
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"get-time.php",
				data:{time:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#time").html(resp);
				}
			});
			
		} else {
			$("#time").html("<span>Please choose...</span>");
		}
		
	});
});
