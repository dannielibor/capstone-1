$(document).ready(function() {
	$("#sec").change(function() { 
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"get-times.php",
				data:{time:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#time").html(resp);
				}
			});
			
		}
		
	});
});
