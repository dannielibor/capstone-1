$(document).ready(function() {
	$("#subj").change(function() { 
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"get-day.php",
				data:{day:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#day").html(resp);
				}
			});
			
		} else {
			$("#day").html("<span>Please choose...</span>");
		}
		
	});
});
