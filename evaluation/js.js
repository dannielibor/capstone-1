$(document).ready(function() {
	$("#prof").change(function() {
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"get-states.php",
				data:{c_id:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#subj").html(resp);
				}
			});
			
		} else {
			$("#subj").html("<option value='' disabled selected>Please choose...</option>");
		}
		
	});
});
