<?php
session_start();
include_once 'includes/db.inc.php';
$_SESSION['source'] = "deanappraisaldept";
?>
<!DOCTYPE html>
<html>

	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<script type="text/javascript" src="jquery.min.js"></script>
	<script type="text/javascript" src="proflistappraisal.js"></script>
	<script type="text/javascript" src="name.js"></script>
	<link rel="stylesheet" type="text/css" href="css.css">
		<title>SBCA - Evaluation Form</title>
		
		<style type = "text/css">
		body{
		padding: 0;
		margin: 0;
		}
		.menu ul{
			list-style: none;
			margin: 0;
		}
		.menu ul li{
		padding: 15px;
		position: relative;
		width: 200px;
		background-color: #333;
		border-top: 1px solid #ffffff;
		border-right: 5px solid #800000;
		}
		.menu ul ul{
		opacity: 0;
		visibility: hidden;
		transition: all 0.3s;
		position: absolute;
		left: 86%;
		top: -1.5%;
		}
		.menu ul li:hover > ul{
		opacity: 1;
		visibility: visible;
		}
		.menu ul li a{
		color: #ffffff;
		text-decoration: none;
		}
		.menu ul li:hover{
		color: #800000;
	background-color: #800000;
	font-size: 120%;
		}
		</style>

	</head>
	
		<body>
		
			<div id="main">
			
			
			<form method = "post" action = "insertAppraisal.php" style = "margin-left:2%;">
			
			<div id = "a">
			<div class = "eval" style = "height:123%; text-align:left;">
			<label style = "font-family:arial;font-size:20px;">DEPARTMENT CHAIR PERFORMANCE APPRAISAL</label><br><br>
			<label>A. PROFESSIONAL RESPONSIBILITIES(70%)</label>
			<table>
				
				<tr>
					<th style = "color:#000000">1.</th>
					<th style = "text-align:left;color:#000000">Engages in professional growth activities</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Attends SBC/CAS seminars, conferences, workshops etc.</td>
					<td><input type = "radio" name = "qa1" value = "5"></td>
					<td><input type = "radio" name = "qa1" value = "4"></td>
					<td><input type = "radio" name = "qa1" value = "3"></td>
					<td><input type = "radio" name = "qa1" value = "2"></td>
					<td><input type = "radio" name = "qa1" value = "1"></td>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Attends off-campus seinars, conferences, workshops etc.</td>
					<td class = "highlight"><input type = "radio" name = "qa2" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qa2" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qa2" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qa2" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qa2" value = "1"></td>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Engages in scholarly research activities.</td>
					<td><input type = "radio" name = "qa3" value = "5"></td>
					<td><input type = "radio" name = "qa3" value = "4"></td>
					<td><input type = "radio" name = "qa3" value = "3"></td>
					<td><input type = "radio" name = "qa3" value = "2"></td>
					<td><input type = "radio" name = "qa3" value = "1"></td>
				</tr>
				
				
				<tr>
					<th style = "color:#000000">2.</th>
					<th style = "text-align:left;color:#000000">Demonstrates dependability in professional duties</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Submites course syllabus that conforms with the CAS curriculum(i.e. for,, content)</td>
					<td class = "highlight"><input type = "radio" name = "qa4" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qa4" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qa4" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qa4" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qa4" value = "1"></td>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Submits a well-constructed examination</td>
					<td><input type = "radio" name = "qa5" value = "5"></td>
					<td><input type = "radio" name = "qa5" value = "4"></td>
					<td><input type = "radio" name = "qa5" value = "3"></td>
					<td><input type = "radio" name = "qa5" value = "2"></td>
					<td><input type = "radio" name = "qa5" value = "1"></td>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Submits accurate grades that conforms with CAS standards.</td>
					<td class = "highlight"><input type = "radio" name = "qa6" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qa6" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qa6" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qa6" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qa6" value = "1"></td>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Submits other requirements prescribed by the department.</td>
					<td><input type = "radio" name = "qa7" value = "5"></td>
					<td><input type = "radio" name = "qa7" value = "4"></td>
					<td><input type = "radio" name = "qa7" value = "3"></td>
					<td><input type = "radio" name = "qa7" value = "2"></td>
					<td><input type = "radio" name = "qa7" value = "1"></td>
				</tr>
				
				
				<tr>
					<th style = "color:#000000">3.</th>
					<th style = "text-align:left;color:#000000">Works cooperatively in bringing about the success of the school program</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Cooperates to bring success of the school program(i.e. spiritual, social, outreack, academic)</td>
					<td class = "highlight"><input type = "radio" name = "qa8" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qa8" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qa8" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qa8" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qa8" value = "1"></td>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Takes care and makes optimum use of the physical and materials resources that supports Instructional program</td>
					<td><input type = "radio" name = "qa9" value = "5"></td>
					<td><input type = "radio" name = "qa9" value = "4"></td>
					<td><input type = "radio" name = "qa9" value = "3"></td>
					<td><input type = "radio" name = "qa9" value = "2"></td>
					<td><input type = "radio" name = "qa9" value = "1"></td>
				</tr>
				
				<tr>
					<th style = "color:#000000">4.</th>
					<th style = "text-align:left;color:#000000">Promptness in meeting obligations</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Has a good and reasonable attendance record</td>
					<td class = "highlight"><input type = "radio" name = "qa10" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qa10" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qa10" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qa10" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qa10" value = "1"></td>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Reports and leaves classes on time</td>
					<td><input type = "radio" name = "qa11" value = "5"></td>
					<td><input type = "radio" name = "qa11" value = "4"></td>
					<td><input type = "radio" name = "qa11" value = "3"></td>
					<td><input type = "radio" name = "qa11" value = "2"></td>
					<td><input type = "radio" name = "qa11" value = "1"></td>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Reports to classes regularly</td>
					<td class = "highlight"><input type = "radio" name = "qa12" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qa12" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qa12" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qa12" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qa12" value = "1"></td>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Attends meetings convocations and other official school invitations regularly</td>
					<td><input type = "radio" name = "qa13" value = "5"></td>
					<td><input type = "radio" name = "qa13" value = "4"></td>
					<td><input type = "radio" name = "qa13" value = "3"></td>
					<td><input type = "radio" name = "qa13" value = "2"></td>
					<td><input type = "radio" name = "qa13" value = "1"></td>
				</tr>
				
			</table>
			</div>
			<!--page2!-->
			<div class = "eval" style = "height:48%;margin-top:72%; text-align:left;">
			<label>B. PROFESSIONAL RELATIONSHIPS(20%)</label>
			<table>
				
				<tr>
					<th style = "color:#000000">1.</th>
					<th style = "text-align:left;color:#000000">Maintains an effective working relationships with staff</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Respects needs and feelings of his/her colleagues</td>
					<td class = "highlight"><input type = "radio" name = "qb1" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qb1" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qb1" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qb1" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qb1" value = "1"></td>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Maintains a positive relationships with all school personnel</td>
					<td><input type = "radio" name = "qb2" value = "5"></td>
					<td><input type = "radio" name = "qb2" value = "4"></td>
					<td><input type = "radio" name = "qb2" value = "3"></td>
					<td><input type = "radio" name = "qb2" value = "2"></td>
					<td><input type = "radio" name = "qb2" value = "1"></td>
				</tr>
				
				<tr>
					<th style = "color:#000000">2.</th>
					<th style = "text-align:left;color:#000000">Maintains a relationships with students that is conductive to learning</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Maintains a supportive, positive and professional relationships with students</td>
					<td class = "highlight"><input type = "radio" name = "qb3" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qb3" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qb3" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qb3" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qb3" value = "1"></td>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Exemplifies academic, moral and ethical norms in his/her personel and professional life</td>
					<td><input type = "radio" name = "qb4" value = "5"></td>
					<td><input type = "radio" name = "qb4" value = "4"></td>
					<td><input type = "radio" name = "qb4" value = "3"></td>
					<td><input type = "radio" name = "qb4" value = "2"></td>
					<td><input type = "radio" name = "qb4" value = "1"></td>
				</tr>
				
			</table>
			</div>
			</div>
			<div id = "b" style = "visibility:hidden;">
			<!--page3!-->
			<div class = "eval" style = "height:93%; text-align:left;">
			<label>C. PERSONAL QUALITIES(10%)</label>
			<table>
				
				<tr>
					<th style = "color:#000000">1.</th>
					<th style = "text-align:left;color:#000000">Health and Vigor</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Displays pleasant personality</td>
					<td class = "highlight"><input type = "radio" name = "qc1" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qc1" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qc1" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qc1" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qc1" value = "1"></td>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Displays a sense of humor</td>
					<td><input type = "radio" name = "qc2" value = "5"></td>
					<td><input type = "radio" name = "qc2" value = "4"></td>
					<td><input type = "radio" name = "qc2" value = "3"></td>
					<td><input type = "radio" name = "qc2" value = "2"></td>
					<td><input type = "radio" name = "qc2" value = "1"></td>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Open to suggestions, new ideas and accepts constructive criticisms</td>
					<td class = "highlight"><input type = "radio" name = "qc3" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qc3" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qc3" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qc3" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qc3" value = "1"></td>
				</tr>
				
				<tr>
					<th style = "color:#000000">2.</th>
					<th style = "text-align:left;color:#000000">Competance and Initiative</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Exercise tact and objectivity in articulating own ideas and concerns</td>
					<td><input type = "radio" name = "qc4" value = "5"></td>
					<td><input type = "radio" name = "qc4" value = "4"></td>
					<td><input type = "radio" name = "qc4" value = "3"></td>
					<td><input type = "radio" name = "qc4" value = "2"></td>
					<td><input type = "radio" name = "qc4" value = "1"></td>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Contributes ideas, inputs for the good of the department and the school</td>
					<td class = "highlight"><input type = "radio" name = "qc5" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qc5" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qc5" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qc5" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qc5" value = "1"></td>
				</tr>
				
				<tr>
					<th style = "color:#000000">3.</th>
					<th style = "text-align:left;color:#000000">Grooming and appropriatness of Attire</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Practicie habits of good grooming</td>
					<td><input type = "radio" name = "qc6" value = "5"></td>
					<td><input type = "radio" name = "qc6" value = "4"></td>
					<td><input type = "radio" name = "qc6" value = "3"></td>
					<td><input type = "radio" name = "qc6" value = "2"></td>
					<td><input type = "radio" name = "qc6" value = "1"></td>
				</tr>
				
				<tr>
					<th style = "color:#000000">4.</th>
					<th style = "text-align:left;color:#000000">Work attitudes</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Is enthusiastic and shows initiative beyond the call of duty</td>
					<td class = "highlight"><input type = "radio" name = "qc7" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qc7" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qc7" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qc7" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qc7" value = "1"></td>
				</tr>
				
				<tr>
					<td/>
					<td style = "color: #800000;">Accepts leadership and fellowships roles</td>
					<td><input type = "radio" name = "qc8" value = "5"></td>
					<td><input type = "radio" name = "qc8" value = "4"></td>
					<td><input type = "radio" name = "qc8" value = "3"></td>
					<td><input type = "radio" name = "qc8" value = "2"></td>
					<td><input type = "radio" name = "qc8" value = "1"></td>
				</tr>
				
				<tr>
					<td class = "highlight"/>
					<td class = "highlight">Upholds the good name and integrity of the school in and out of the school premises</td>
					<td class = "highlight"><input type = "radio" name = "qc9" value = "5"></td>
					<td class = "highlight"><input type = "radio" name = "qc9" value = "4"></td>
					<td class = "highlight"><input type = "radio" name = "qc9" value = "3"></td>
					<td class = "highlight"><input type = "radio" name = "qc9" value = "2"></td>
					<td class = "highlight"><input type = "radio" name = "qc9" value = "1"></td>
				</tr>
				
			</table>
			</div>
			<button class="sign" style = "margin-left:45%;margin-right:25%;width:10%;margin-top:58%;position:absolute" name = "submit_dean">
				<span>Submit </span></button>
			</div>
			
			</form>
			
		
			<header>
			
				<div class = "container">
				
					<div class = "asd">
					<label id = "name" style = "position:fixed;margin-top:3%;margin-left:50%;font-family:arial;color:#ffffff;font-size:20px;"></label>
						<div id="mySidenav" class="sidenav">
						
							<img src="background.jpg" height = "100" width = "100" class = "picture" style="cursor:pointer">
							<a href = "dean.php" style = "font-size:13px">Home</a>
							<!--<a href = "profile.php" style = "font-size:13px">Profile</a>!-->
							<a href = "appraisalDean.php" style = "font-size:13px">Faculty Performance Appraisal</a>
							<a href = "deptappraisalDean.php" style = "font-size:13px">Department Chair Performance Appraisal</a>
							<a href = "viceappraisalDean.php" style = "font-size:13px">Vice Dean Performance Appraisal</a>
							<a href = "observeDean.php" style = "font-size:13px">Faculty Classroom Observation</a>
							<a href = "deptobserveDean.php" style = "font-size:13px">Department Chair Classroom Observation</a>
							<a href = "viceobserveDean.php" style = "font-size:13px">Vice Dean Classroom Observation</a>
							<a href = "statDean.php" style = "font-size:13px">Statistics</a>
							<!--<a href = "stats.php" style = "font-size:13px">Department Chair Statistics</a>!-->
							<!--<a href = "settings.php" style = "font-size:13px">Settings</a>!-->
							<a onclick = "document.getElementById('id02').style.display='block'" style = "cursor:pointer;font-size:13px">Logout</a>
						
						</div>
						
						<img src="btnNav.jpg" alt="Navigation" height = "85dp" class = "menu" onclick="openNav()" style="cursor:pointer">
		
						<a href = "account.php"><img src="logo.png" alt="San Beda College Alabang" height = "70dp" class = "logo" style="cursor:pointer"></a>
					
						<font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">San Beda College Alabang</font>
						
						<div class = "menu" style = "margin-left:-3%;margin-top:6.7%;height:100%;width:20%;
			font-family:arial;font-size:20px;float:left;position:absolute">
						
						<ul>
						<?php
						$course = "select * from tblcor group by department asc";
						$rescourse = mysqli_query($conn,$course);
						while($rowcourse = mysqli_fetch_assoc($rescourse)){
							
							$sql1 = "select * from tblcor where department = '".$rowcourse['department']."' and not facultyID = '".$_SESSION['prof']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."' group by facultyID";
							$select1 = mysqli_query($conn,$sql1);
							$row1 = mysqli_fetch_assoc($select1);
							
							if($row1 < 1){
							}else{
							echo "<li><a href = '#'>".$row1['department']."</a> <ul id = 'sample' name = 'sample'>";
							
							$sql = "select * from tblcor where department = '".$rowcourse['department']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."' group by facultyID";
							$select = mysqli_query($conn,$sql);
							while($row = mysqli_fetch_assoc($select)){
							$query = "select * from tblprofessor where facultyID = '".$row['facultyID']."'";
							$res = mysqli_query($conn,$query);
							$row1 = mysqli_fetch_assoc($res);
							
							$user = "select * from tbluser where userID = '".$row1['userID']."' and role = 'dept.chair' and not role = 'vicedean'";
							$result = mysqli_query($conn,$user);
							$row2 = mysqli_fetch_assoc($result);
							
							if($row2 < 1){
							}else{
								echo "<li value = '".$row['facultyID']."'><a href = '#'>".$row2['firstname']." ".$row2['lastname']."</a></li>";
							}
							
							}
							echo "</ul></li>";
							}

							

							
						}
						?>
						</ul>
						
						</div>

					</div>
					
						<nav class = "nav">
							<ul class = "ul">
								<li class = "li"><a class = "a" href = "#"><?php echo $_SESSION['firstname'] ?></a></li>
							</ul>
						</nav>
						
						<div class = "eval" style = "margin-top:10%;position:fixed;height:30%;margin-left:80%;width:15%;text-align:left">
				
			<label>RATING INTERPRETATION:</label><br><br>
<label>5 = Outstanding</label><br><br>
<label>4 = Very Satisfactory</label><br><br>
<label>3 = Satisfactory</label><br><br>
<label>2 = Moderate Satisfactory</label><br><br>
<label>1 = Needs Improvement</label>			
			
			</div>
					
				</div>
				
			</div>
			
			</header>
	
			<footer class = "footerhome">
			
			<div class="pagination" style = "margin-left:40%">
  <a href="#" id = "back">&laquo;</a>
  <a href="#" value = "1" id = "p1">1</a>
  <a href="#" value = "2" id = "p2">2</a>
  <a href="#" id = "next">&raquo;</a>
  
  <script>
  var p1 = document.getElementById('p1');
  var p2 = document.getElementById('p2');
  var back = document.getElementById('back');
  var next = document.getElementById('next');
  var num = 1; 
  
  (function() {
        next.onclick = function() { 
			if(num == 1){
			document.getElementById("b").style.visibility = "visible";
			document.getElementById("a").style.visibility = "hidden"; num = 2; 
			}else if (num == 2){
			document.getElementById("a").style.visibility = "visible"; num = 1;
			document.getElementById("b").style.visibility = "hidden";
			}return num;
        };
    })(); 
	
	(function() {
        back.onclick = function() { 
			if(num == 1){
			document.getElementById("b").style.visibility = "hidden";
			document.getElementById("a").style.visibility = "visible"; num = 1;
			}else if (num == 2){
			document.getElementById("a").style.visibility = "visible"; num = 1;
			document.getElementById("b").style.visibility = "hidden";
			}return num;
        };
    })(); 
	
	(function() {
        p1.onclick = function() { 
		document.getElementById("b").style.visibility = "hidden";
			document.getElementById("a").style.visibility = "visible"; num = 1;
        };
    })();
(function() {
        p2.onclick = function() { 
			document.getElementById("b").style.visibility = "visible";
			document.getElementById("a").style.visibility = "hidden"; num = 2;
        };
    })();	
	
  </script>
  
  </div>
			
				<div class = "follow" style = "bottom:20%">
				
					<p class = "bottom">FOLLOW US</p>
					
					<ul class = "link">
					
						<li><a href="https://www.google.com.ph/?gfe_rd=cr&dcr=0&ei=12WNWqO6G9GiX_j3gqgJ" target="_blank">
							<image src = "google.png" height = "10"> Google+</a></li><br>
						
						<li><a href="https://twitter.com/sbcasec?lang=en" target="_blank">
							<image src = "twitter.png" height = "10"> Twitter</a></li><br>
							
						<li><a href="https://www.facebook.com/BedaAlabang/" target="_blank">
							<image src = "facebook.png" height = "10"> Facebook</a></li><br>
							
						<li><a href="https://www.instagram.com/?hl=en" target="_blank">
							<image src = "instagram.png" height = "10"> Instagram</a></li><br>
							
						<li><a href="https://www.youtube.com" target="_blank"><image src = "youtube.png" height = "10"> Youtube</a></li>
					
					</ul>
					
				</div>
				
				<div class = "contactUs">
				
					<p class = "bottom">CONTACT US</p>
					
					<ul class = "link">
					
						<li><a class = "aaaa" href="http://www.sanbeda-alabang.edu.ph/bede/" target="_blank">
							Website: http://www.sanbeda-alabang.edu.ph</a></li><br>
							
						<li><a class = "aaaa" href="#home" style = "cursor:auto">E-mail: sbca@sanbeda-alabang.edu.ph</a></li><br>
						
						<li><a class = "aaaa" href="#home" style = "cursor:auto">Telephone: 236-7222</a></li><br>
					
					</ul>
					
				</div>
				
				<div class = "copyright">
					<p id = "copyright"/>
				</div>
	
			</footer>
			
	<script>
		var d = new Date().getFullYear();
		document.getElementById("copyright").innerHTML = "Copyright &copy; " + d + " San Beda College Alabang, All Rights Reserved.";
	</script>
	
			<div id="id02" class="logout">
  
				<form class="logout-content animate" method = "post" action = "includes/logout.php">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('id02').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
						<label for="username"><b class = "align">Are you sure?</b></label><br>
						<button class="button1" name = "logout"><span>Logout </span></button>
					</div>
			</div>

	<script>
	
		var logout = document.getElementById('id02');
		window.onclick = function(event) {
			if (event.target == logout) {
				logout.style.display = "none";
			}
		}
		
		var x = true;
		function openNav() {
			if(x == true) {
				document.getElementById("mySidenav").style.width = "197px";
				document.getElementById("main").style.marginLeft = "197px";
				x = false;
			}
			else {
				closeNav();
				x = true;
			}
		}

		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
			document.getElementById("main").style.marginLeft= "0";
			document.body.style.backgroundColor = "white";
		}
		
	</script>

		</body>
		
</html>