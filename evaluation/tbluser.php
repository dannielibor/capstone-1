<?php
session_start();
include_once 'includes/db.inc.php';
?>
<!DOCTYPE html>
<html>

	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="stylesheet" type="text/css" href="css.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
		<title>SBCA - Evaluation Form</title>
	</head>
	
		<body>
		
			<div id="main">
			
			<form action = "admin.php" style = "top:15%;position:absolute">
			<input type = "submit" name = "user" value = "back"/>
			</form>
			
			<form method = "post" class = "form-table" style = "position:absolute;top:20%">
			
			<caption>Users<img src="redlionlogo.png" width="5%" align="top"></img></caption>
			
			<input type = "submit" name = "user" value = "delete" style = "float:left" action = "includes/delete.php"/>
			<input type = "text" placeholder = "Search..." style = "float:right"/>

			<table class = "tblaccount">
				<tr class = "tblaccount" style = "background-color:#800000">
					<th>Select</th>
					<th>ID</th>
					<th>First name</th>
					<th>Last name</th>
					<th>Gender</th>
					<th>E-mail</th>
					<th>Action</th>
				</tr>
				<?php 
				$query = "SELECT * FROM tbluser ORDER BY userID ASC";
				$result = mysqli_query($conn, $query);
				while($row = mysqli_fetch_assoc($result)){
				$id = $row['userID'];
				$fname = $row['firstname'];
				$lname = $row['lastname'];
				$gen = $row['gender'];
				$mail = $row['email'];
				?>
					<tr class = "tblaccount">
						<td><input type = "checkbox" name = "select[]" value = "<?php echo $id; ?>"/></td>
						<td><?php echo $id; ?></td>
						<td><?php echo $fname; ?></td>
						<td><?php echo $lname; ?></td>
						<td><?php echo $gen; ?></td>
						<td><?php echo $mail; ?></td>
						<td><button value = "<?php echo $id; ?>" name = "view" onclick = "document.getElementById('view').style.display='block'" style = "cursor:pointer">view</button> | 
						<button value = "<?php echo $id; ?>" name = "edit" onclick = "document.getElementById('edit').style.display='block'" style = "cursor:pointer">edit</button></td>
					</tr>
					
				<?php } ?>	
			</table>
			
			</form>
			
			
			<header>
			
				<div class = "container">
				
					<div class = "asd">
					
						<div id="mySidenav" class="sidenav">
						
							<img src="background.jpg" alt="<?php echo $username; ?>" height = "100" width = "100" class = "picture" style="cursor:pointer">
							<a href = "admin.php">Home</a>
							<a href = "importCor.php">Import COR</a>
							<a href = "importChair.php">Import Program Chairs</a>
							<a href = "importDean.php">Import Dean & Vice Dean</a>
							<a href = "importBanner.php">Import Banners</a>
							<a onclick = "document.getElementById('id02').style.display='block'" style = "cursor:pointer">Logout</a>
						
						</div>
						
						<img src="btnNav.jpg" alt="Navigation" height = "85dp" class = "menu" onclick="openNav()" style="cursor:pointer">
		
						<a href = "admin.php"><img src="logo.png" alt="San Beda College Alabang" height = "70dp" class = "logo" style="cursor:pointer"></a>
					
						<font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">San Beda College Alabang</font>

					</div>
					
						<nav class = "nav">
							<ul class = "ul">
								<li class = "li"><a class = "a" href = "#"><?php echo $_SESSION['firstname']; ?></a></li>
							</ul>
						</nav>
						
				</div>
				
			</div>
			
			</header>
			
	
			<footer class = "footerhome">
			
				<div class = "follow">
				
					<p class = "bottom">FOLLOW US</p>
					
					<ul class = "link">
					
						<li><a href="https://www.google.com.ph/?gfe_rd=cr&dcr=0&ei=12WNWqO6G9GiX_j3gqgJ" target="_blank">
							<image src = "google.png" height = "10"> Google+</a></li><br>
						
						<li><a href="https://twitter.com/sbcasec?lang=en" target="_blank">
							<image src = "twitter.png" height = "10"> Twitter</a></li><br>
							
						<li><a href="https://www.facebook.com/BedaAlabang/" target="_blank">
							<image src = "facebook.png" height = "10"> Facebook</a></li><br>
							
						<li><a href="https://www.instagram.com/?hl=en" target="_blank">
							<image src = "instagram.png" height = "10"> Instagram</a></li><br>
							
						<li><a href="https://www.youtube.com" target="_blank"><image src = "youtube.png" height = "10"> Youtube</a></li>
					
					</ul>
					
				</div>
				
				<div class = "contactUs">
				
					<p class = "bottom">CONTACT US</p>
					
					<ul class = "link">
					
						<li><a class = "aaaa" href="http://www.sanbeda-alabang.edu.ph/bede/" target="_blank">
							Website: http://www.sanbeda-alabang.edu.ph</a></li><br>
							
						<li><a class = "aaaa" href="#home" style = "cursor:auto">E-mail: sbca@sanbeda-alabang.edu.ph</a></li><br>
						
						<li><a class = "aaaa" href="#home" style = "cursor:auto">Telephone: 236-7222</a></li><br>
					
					</ul>
					
				</div>
				
				<div class = "copyright">
					<p id = "copyright"/>
				</div>
	
			</footer>
			
	<script>
		var d = new Date().getFullYear();
		document.getElementById("copyright").innerHTML = "Copyright &copy; " + d + " San Beda College Alabang, All Rights Reserved.";
	</script>
	
			<div id="id02" class="logout">
  
				<form class="logout-content animate" method = "post" action = "home.php">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('id02').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
						<label for="username"><b class = "align">Are you sure?</b></label><br>
						<button class="button1" ><span>Logout </span></button>
					</div>
			</div>

	<script>
	
		var logout = document.getElementById('id02');
		window.onclick = function(event) {
			if (event.target == logout) {
				logout.style.display = "none";
			}
		}
		
		var x = true;
		function openNav() {
			if(x == true) {
				document.getElementById("mySidenav").style.width = "197px";
				document.getElementById("main").style.marginLeft = "197px";
				x = false;
			}
			else {
				closeNav();
				x = true;
			}
		}

		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
			document.getElementById("main").style.marginLeft= "0";
			document.body.style.backgroundColor = "white";
		}
		
	</script>

		</body>
		
</html>