$(document).ready(function() {
	$("#year").change(function() {
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"sum-year.php",
				data:{sum:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#sem").html(resp);
				}
			});
			
		} else {
			$("#sem").html("<option value='' disabled selected>Please choose...</option>");
		}
		
	});
});

