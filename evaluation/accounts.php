<?php
session_start();
include_once 'includes/db.inc.php';
?>
<!DOCTYPE html>
<html>

	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<title>SBCA - Evaluation Form</title>
	</head>
	
		<body>
		
			<video autoplay muted loop id="myVideo">
			<source src="sbca.mp4" type="video/mp4">
			Your browser does not support HTML5 video.
			</video>
			
			<div id="main">
			
			<form method = "post" action = "includes/delete.php" class = "form-table">
			
			<div class = "account" id = "a">
			
			<caption>Users<img src="redlionlogo.png" width="5%" align="top"></img></caption>
			
			<table class = "tblaccount">
				<tr class = "tblaccount" style = "background-color:#800000">
					<th>Select</th>
					<th>ID</th>
					<th>First name</th>
					<th>Last name</th>
					<th>Gender</th>
					<th>E-mail</th>
					<th>Birthday</th>
					<th>Action</th>
				</tr>
				<?php 
				$query = "SELECT * FROM tbluser ORDER BY userID ASC";
				$result = mysqli_query($conn, $query);
				while($row = mysqli_fetch_assoc($result)): 
				$id = $row['userID'];
				$fname = $row['firstname'];
				$lname = $row['lastname'];
				$gen = $row['gender'];
				$mail = $row['email'];
				$bday = $row['bday'];
				?>
					<tr class = "tblaccount">
						<td><input type = "checkbox" name = "select[]" value = "<?php echo $id; ?>"/></td>
						<td><?php echo $id; ?></td>
						<td><?php echo $fname; ?></td>
						<td><?php echo $lname; ?></td>
						<td><?php echo $gen; ?></td>
						<td><?php echo $mail; ?></td>
						<td><?php echo $bday; ?></td>
						<td><button value = "<?php echo $id; ?>" name = "view" onclick = "document.getElementById('view').style.display='block'" style = "cursor:pointer">view</button> | 
						<button value = "<?php echo $id; ?>" name = "edit" onclick = "document.getElementById('edit').style.display='block'" style = "cursor:pointer">edit</button></td>
					</tr>
					
				<?php endwhile; ?>	
			</table>
			
			
			<div id="view" class="login">
				
					<div class="imgcontainerlogin">
						<span onclick="document.getElementById('view').style.display='none'" class="close" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogin">
					
						<?php
			?>
						
					</div>
				
			</div>
			
			
			
			<div id="edit" class="signup_student">
  
				<form class="logout-content animate">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('edit').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
					<?php
			?>
					</div>
					
				</form>	
				
			</div>
			
			<input type = "submit" name = "user" value = "delete" style = "float:left"/>
			
			</div>
			
			</form>
			
			<!--students!-->
			
			<form method = "post" action = "includes/delete.php" class = "form-table">
			
			<div class = "account" id = "b" style = "visibility:hidden">
			
			<caption>Students<img src="redlionlogo.png" width="5%" align="top"></img></caption>
			
			<table class = "tblaccount">
				<tr class = "tblaccount" style = "background-color:#800000">
					<th>Select</th>
					<th>studentID</th>
					<th>First name</th>
					<th>Last name</th>
					<th>course</th>
					<th>Action</th>
				</tr>
				<?php 
				
				$query = "select * from tblstudent order by studentID asc";
				$res = mysqli_query($conn,$query);
				
				while($rowstud = mysqli_fetch_assoc($res)){
					$userid = $rowstud['userID'];
				$id = $rowstud['studentID'];
				$select = "select * from tblcor where studentID = '".$id."'";
				$result = mysqli_query($conn,$select);
				$row = mysqli_fetch_assoc($result);
				$course = $row['course'];
				
				$sql = "select * from tbluser where userID = '".$userid."'";
				$name = mysqli_query($conn,$sql);
				$rowname = mysqli_fetch_assoc($name);
				$fname = $rowname['firstname'];
				$lname = $rowname['lastname'];
				
				?>
					<tr class = "tblaccount">
						<td><input type = "checkbox" name = "selectstud[]" value = "<?php echo $userid; ?>"/></td>
						<td><?php echo $id; ?></td>
						<td><?php echo $fname; ?></td>
						<td><?php echo $lname; ?></td>
						<td><?php echo $course; ?></td>
						<td><button value = "<?php echo $id; ?>" name = "view" onclick = "document.getElementById('view').style.display='block'" style = "cursor:pointer">view</button> | 
						<button value = "<?php echo $id; ?>" name = "edit" onclick = "document.getElementById('edit').style.display='block'" style = "cursor:pointer">edit</button></td>
					</tr>
					
				<?php } ?>	
			</table>
			
			
			<div id="view" class="login">
				
					<div class="imgcontainerlogin">
						<span onclick="document.getElementById('view').style.display='none'" class="close" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogin">
					
						<?php
			?>
						
					</div>
				
			</div>
			
			
			
			<div id="edit" class="signup_student">
  
				<form class="logout-content animate">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('edit').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
					<?php
			?>
					</div>
					
				</form>	
				
			</div>
			<input type = "submit" name = "student" value = "delete" style = "float: left"/>
			</div>
			</form>
			<!--professor!-->
			
			<div class = "account" id = "c" style = "visibility:hidden">
			
			<caption>Professor<img src="redlionlogo.png" width="5%" align="top"></img></caption>
			
			<form method = "post" action = "includes/delete.php" class = "form-table">
			
			<table class = "tblaccount">
				<tr class = "tblaccount" style = "background-color:#800000">
					<th>Select</th>
					<th>facultyID</th>
					<th>First name</th>
					<th>Last name</th>
					<th>department</th>
					<th>Action</th>
				</tr>
				<?php 
				
				$select = "select * from tblprofessor order by facultyID asc";
				$result = mysqli_query($conn,$select);
				while($row = mysqli_fetch_assoc($result)){
					
					$id = $row['userID'];
					$facid = $row['facultyID'];
					
					$query = "select * from tblcor where facultyID = '".$facid."'";
					$res = mysqli_query($conn,$query);
					$rowcor = mysqli_fetch_assoc($res);
					
					$dep = $rowcor['department'];
					
					$sql = "select * from tbluser where userID = '".$id."'";
					$resname = mysqli_query($conn,$sql);
					$rowname = mysqli_fetch_assoc($resname);
					
					$fname = $rowname['firstname'];
					$lname = $rowname['lastname'];
				
				?>
					<tr class = "tblaccount">
						<td><input type = "checkbox" name = "select[]" value = "<?php echo $id; ?>"/></td>
						<td><?php echo $facid; ?></td>
						<td><?php echo $fname; ?></td>
						<td><?php echo $lname; ?></td>
						<td><?php echo $dep; ?></td>
						<td><button value = "<?php echo $id; ?>" name = "edit" onclick = "document.getElementById('edit').style.display='block'" style = "cursor:pointer">edit</button></td>
					</tr>
					
				<?php } ?>	
			</table>
			
			<div id="view" class="login">
				
					<div class="imgcontainerlogin">
						<span onclick="document.getElementById('view').style.display='none'" class="close" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogin">
					
						<?php
			?>
						
					</div>
				
			</div>
			
			<div id="edit" class="signup_student">
  
				<form class="logout-content animate">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('edit').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
					<?php
			?>
					</div>
					
				</form>	
				
			</div>
			<input type = "submit" name = "prof" value = "delete" style = "float: left"/>
			</div>
			</form>

			<!--dept!-->
			<form method = "post" action = "includes/delete.php" class = "form-table">
			
			<div class = "account" id = "d" style = "visibility:hidden;width:50%">
			
			<caption>Department Chairs<img src="redlionlogo.png" width="5%" align="top"></img></caption>
			
			<table class = "tblaccount">
				<tr class = "tblaccount" style = "background-color:#800000">
					<th>Select</th>
					<th>facultyID</th>
					<th>First name</th>
					<th>Last name</th>
					<th>department</th>
					<th>Action</th>
				</tr>
				<?php 
				
				$select = "select * from tbluser where role = 'dept.chair' order by userID asc";
				$res = mysqli_query($conn,$select);
				while($row = mysqli_fetch_assoc($res)){
					
					$id = $row['userID'];
					$fname = $row['firstname'];
					$lname = $row['lastname'];
					
					$query = "select * from tblprofessor where userID = '".$id."'";
					$result = mysqli_query($conn,$query);
					$rowprof = mysqli_fetch_assoc($result);
					
					$facid = $rowprof['facultyID'];
					
					$sql = "select * from tblcor where facultyID = '".$facid."'";
					$resql = mysqli_query($conn,$sql);
					$rowsql = mysqli_fetch_assoc($resql);
					
					$dep = $rowsql['department'];
				
				?>
					<tr class = "tblaccount">
					<td><input type = "checkbox" name = "select[]" value = "<?php echo $id; ?>"/></td>
						<td><?php echo $facid; ?></td>
						<td><?php echo $fname; ?></td>
						<td><?php echo $lname; ?></td>
						<td><?php echo $dep; ?></td>
						<td><button value = "<?php echo $id; ?>" name = "edit" onclick = "document.getElementById('edit').style.display='block'" style = "cursor:pointer">edit</button></td>
					</tr>
					
				<?php } ?>	
			</table>
			
			<div id="view" class="login">
				
					<div class="imgcontainerlogin">
						<span onclick="document.getElementById('view').style.display='none'" class="close" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogin">
					
						<?php
			?>
						
					</div>
				
			</div>
			
			
			
			<div id="edit" class="signup_student">
  
				<form class="logout-content animate">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('edit').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
					<?php
			?>
					</div>
					
				</form>	
				
			</div>
			<input type = "submit" name = "dept" value = "delete" style = "float: left"/>
			</div>
			</form>
			
			<!--vice/dean!-->
			<form method = "post" action = "includes/delete.php" class = "form-table">
			
			<div class = "account" id = "e" style = "visibility:hidden;width:50%">
			
			<caption>Dean & Vice Dean<img src="redlionlogo.png" width="5%" align="top"></img></caption>
			
			<table class = "tblaccount">
				<tr class = "tblaccount" style = "background-color:#800000">
					<th>Select</th>
					<th>userID</th>
					<th>First name</th>
					<th>Last name</th>
					<th>role</th>
					<th>Action</th>
				</tr>
				<?php 
				
				$select = "select * from tbluser where role = 'vice dean' or role = 'dean'";
				$result = mysqli_query($conn,$select);
				while($row = mysqli_fetch_assoc($result)){
				$userid = $row['userID'];
				$fname = $row['firstname'];
				$lname = $row['lastname'];
				$role = $row['role'];
				?>
					<tr class = "tblaccount">
					<td><input type = "checkbox" name = "select[]" value = "<?php echo $userid; ?>"/></td>
						<td><?php echo $userid; ?></td>
						<td><?php echo $fname; ?></td>
						<td><?php echo $lname; ?></td>
						<td><?php echo $role; ?></td>
						<td><button value = "<?php echo $id; ?>" name = "edit" onclick = "document.getElementById('edit').style.display='block'" style = "cursor:pointer">edit</button></td>
					</tr>
					
				<?php } ?>	
			</table>
			
			<div id="view" class="login">
				
					<div class="imgcontainerlogin">
						<span onclick="document.getElementById('view').style.display='none'" class="close" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogin">
					
						<?php
			?>
						
					</div>
				
			</div>
			
			
			
			<div id="edit" class="signup_student">
  
				<form class="logout-content animate">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('edit').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
					<?php
			?>
					</div>
					
				</form>	
				
			</div>
			<input type = "submit" name = "dean" value = "delete" style = "float: left"/>
			</div>
			</form>
			
			<!--schedule!-->
			
			<div class = "account" id = "f" style = "visibility:hidden">
			
			<caption>Schedule<img src="redlionlogo.png" width="5%" align="top"></img></caption>
			
			<table class = "tblaccount">
				<tr class = "tblaccount" style = "background-color:#800000">
					<th>scheduleID</th>
					<th>facultyID</th>
					<th>subject/code</th>
					<th>year/section</th>
					<th>day</th>
					<th>time</th>
					<th>room</th>
					<th>Action</th>
				</tr>
				<?php 
				$query = "SELECT * FROM tblcor ORDER BY corID ASC";
				$result = mysqli_query($conn, $query);
				while($row = mysqli_fetch_assoc($result)): 
				$id = $row['corID'];
				$facid = $row['facultyID'];
				$subj = $row['subjectCode'];
				$yr = $row['yrsec'];
				$day = $row['day'];
				$time = $row['time'];
				$room = $row['room'];
				?>
					<tr class = "tblaccount">
						<td><?php echo $id; ?></td>
						<td><?php echo $facid; ?></td>
						<td><?php echo $subj; ?></td>
						<td><?php echo $yr; ?></td>
						<td><?php echo $day; ?></td>
						<td><?php echo $time; ?></td>
						<td><?php echo $room; ?></td>
						<td><button value = "<?php echo $id; ?>" name = "view" onclick = "document.getElementById('view').style.display='block'" style = "cursor:pointer">view</button></td>
					</tr>
					
				<?php endwhile;?>	
			</table>
			
			<div id="view" class="login">
				
					<div class="imgcontainerlogin">
						<span onclick="document.getElementById('view').style.display='none'" class="close" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogin">
					
						<?php
			?>
						
					</div>
				
			</div>
			
			
			
			<div id="edit" class="signup_student">
  
				<form class="logout-content animate">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('edit').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
					<?php
			?>
					</div>
					
				</form>	
				
			</div>
			</div>
			
			<header>
			
				<div class = "container">
				
					<div class = "asd">
					
						<div id="mySidenav" class="sidenav">
						
							<img src="background.jpg" alt="<?php echo $username; ?>" height = "100" width = "100" class = "picture" style="cursor:pointer">
							<a href = "admin.php">Home</a>
							<a href = "accounts.php">Accounts</a>
							<a href = "settingsadmin.php">Settings</a>
							<a onclick = "document.getElementById('id02').style.display='block'" style = "cursor:pointer">Logout</a>
						
						</div>
						
						<img src="btnNav.jpg" alt="Navigation" height = "85dp" class = "menu" onclick="openNav()" style="cursor:pointer">
		
						<a href = "account.php"><img src="logo.png" alt="San Beda College Alabang" height = "70dp" class = "logo" style="cursor:pointer"></a>
					
						<font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">San Beda College Alabang</font>

					</div>
					
						<nav class = "nav">
							<ul class = "ul">
								<li class = "li"><a class = "a" href = "#"><?php echo $_SESSION['firstname']; ?></a></li>
							</ul>
						</nav>
						
						<nav class = "tabs">
							<ul class = "tabs_ul">
								<li class = "tabs_li"><a class = "tabs_a" href = "#" value = "user" id = "user">Users</a></li>
								<li class = "tabs_li"><a class = "tabs_a"  href = "#" id = "student" value = "student">Students</a></li>
								<li class = "tabs_li"><a class = "tabs_a"  href = "#" id = "prof" value = "prof">Professors</a></li>
								<li class = "tabs_li"><a class = "tabs_a"  href = "#" id = "dept" value = "dept">Department Chairs</a></li>
								<li class = "tabs_li"><a class = "tabs_a"  href = "#" id = "dean" value = "dean">Dean & Vice Dean</a></li>
								<li class = "tabs_li"><a class = "tabs_a"  href = "#" id = "sched" value = "sched">Schedules</a></li>
							</ul>
						</nav>
						
						<script>
						var a = document.getElementById('user');
						var b = document.getElementById('student');
						var c = document.getElementById('prof');
						var d = document.getElementById('dept');
						var e = document.getElementById('dean');
						var f = document.getElementById('sched');
						
						(function() {
        f.onclick = function() { 
		document.getElementById("a").style.visibility = "hidden";
		document.getElementById("b").style.visibility = "hidden";
		document.getElementById("c").style.visibility = "hidden";
			document.getElementById("d").style.visibility = "hidden";
			document.getElementById("e").style.visibility = "hidden";
			document.getElementById("f").style.visibility = "visible";
        };
    })();
						
						(function() {
        e.onclick = function() { 
		document.getElementById("a").style.visibility = "hidden";
		document.getElementById("b").style.visibility = "hidden";
		document.getElementById("c").style.visibility = "hidden";
			document.getElementById("d").style.visibility = "hidden";
			document.getElementById("e").style.visibility = "visible";
			document.getElementById("f").style.visibility = "hidden";
        };
    })();
						
						(function() {
        d.onclick = function() { 
		document.getElementById("a").style.visibility = "hidden";
		document.getElementById("b").style.visibility = "hidden";
		document.getElementById("c").style.visibility = "hidden";
			document.getElementById("d").style.visibility = "visible";
			document.getElementById("e").style.visibility = "hidden";
			document.getElementById("f").style.visibility = "hidden";
        };
    })();
						
							(function() {
        a.onclick = function() { 
		document.getElementById("a").style.visibility = "visible";
		document.getElementById("b").style.visibility = "hidden";
		document.getElementById("c").style.visibility = "hidden";
			document.getElementById("d").style.visibility = "hidden";
			document.getElementById("e").style.visibility = "hidden";
			document.getElementById("f").style.visibility = "hidden";
        };
    })();
				
(function() {
        b.onclick = function() { 
		document.getElementById("a").style.visibility = "hidden";
		document.getElementById("b").style.visibility = "visible";
		document.getElementById("c").style.visibility = "hidden";
			document.getElementById("d").style.visibility = "hidden";
			document.getElementById("e").style.visibility = "hidden";
			document.getElementById("f").style.visibility = "hidden";
        };
    })();
	
	(function() {
        c.onclick = function() { 
		document.getElementById("a").style.visibility = "hidden";
		document.getElementById("b").style.visibility = "hidden";
		document.getElementById("c").style.visibility = "visible";
			document.getElementById("d").style.visibility = "hidden";
			document.getElementById("e").style.visibility = "hidden";
			document.getElementById("f").style.visibility = "hidden";
        };
    })();
				
				</script>
						
				</div>
				
			</div>
			
			</header>
	
			<footer class = "footerhome">
			
				<div class = "follow">
				
					<p class = "bottom">FOLLOW US</p>
					
					<ul class = "link">
					
						<li><a href="https://www.google.com.ph/?gfe_rd=cr&dcr=0&ei=12WNWqO6G9GiX_j3gqgJ" target="_blank">
							<image src = "google.png" height = "10"> Google+</a></li><br>
						
						<li><a href="https://twitter.com/sbcasec?lang=en" target="_blank">
							<image src = "twitter.png" height = "10"> Twitter</a></li><br>
							
						<li><a href="https://www.facebook.com/BedaAlabang/" target="_blank">
							<image src = "facebook.png" height = "10"> Facebook</a></li><br>
							
						<li><a href="https://www.instagram.com/?hl=en" target="_blank">
							<image src = "instagram.png" height = "10"> Instagram</a></li><br>
							
						<li><a href="https://www.youtube.com" target="_blank"><image src = "youtube.png" height = "10"> Youtube</a></li>
					
					</ul>
					
				</div>
				
				<div class = "contactUs">
				
					<p class = "bottom">CONTACT US</p>
					
					<ul class = "link">
					
						<li><a class = "aaaa" href="http://www.sanbeda-alabang.edu.ph/bede/" target="_blank">
							Website: http://www.sanbeda-alabang.edu.ph</a></li><br>
							
						<li><a class = "aaaa" href="#home" style = "cursor:auto">E-mail: sbca@sanbeda-alabang.edu.ph</a></li><br>
						
						<li><a class = "aaaa" href="#home" style = "cursor:auto">Telephone: 236-7222</a></li><br>
					
					</ul>
					
				</div>
				
				<div class = "copyright">
					<p id = "copyright"/>
				</div>
	
			</footer>
			
	<script>
		var d = new Date().getFullYear();
		document.getElementById("copyright").innerHTML = "Copyright &copy; " + d + " San Beda College Alabang, All Rights Reserved.";
	</script>
	
			<div id="id02" class="logout">
  
				<form class="logout-content animate" method = "post" action = "home.php">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('id02').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
						<label for="username"><b class = "align">Are you sure?</b></label><br>
						<button class="button1" ><span>Logout </span></button>
					</div>
			</div>

	<script>
		var view = document.getElementById('view');
		var edit = document.getElementById('edit');
		var logout = document.getElementById('id02');
		window.onclick = function(event) {
			if (event.target == logout) {
				logout.style.display = "none";
			}
			if (event.target == view) {
				view.style.display = "none";
				}
			if (event.target == edit) {
				edit.style.display = "none";
				}	
		}
		
		var x = true;
		function openNav() {
			if(x == true) {
				document.getElementById("mySidenav").style.width = "197px";
				document.getElementById("main").style.marginLeft = "197px";
				x = false;
			}
			else {
				closeNav();
				x = true;
			}
		}

		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
			document.getElementById("main").style.marginLeft= "0";
			document.body.style.backgroundColor = "white";
		}
		
	</script>

		</body>
		
</html>