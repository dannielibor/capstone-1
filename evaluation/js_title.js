$(document).ready(function() {
	$("#crs").change(function() { 
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"get-title.php",
				data:{title:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#title").html(resp);
				}
			});
			
		}
		
	});
});
