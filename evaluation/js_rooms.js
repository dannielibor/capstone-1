$(document).ready(function() {
	$("#sec").change(function() { 
		var userID = $(this).val();
		if(userID != "") {
			$.ajax({
				url:"get-rooms.php",
				data:{room:userID},
				type:'POST',
				success:function(response) {
					var resp = $.trim(response);
					$("#room").html(resp);
				}
			});
			
		}
		
	});
});
