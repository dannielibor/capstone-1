<?php
session_start();
include_once 'includes/db.inc.php';
$_SESSION['source'] = "";
?>
<!DOCTYPE html>
<html>

	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<script type="text/javascript" src="jquery.min.js"></script>
		<script type="text/javascript" src="name.js"></script>
<script type="text/javascript" src="js_cor.js"></script>
		<title>SBCA - Evaluation Form</title>
		
		<style type = "text/css">
		body{
		padding: 0;
		margin: 0;
		}
		.menu ul{
			list-style: none;
			margin: 0;
		}
		.menu ul li{
		padding: 15px;
		position: relative;
		width: 200px;
		background-color: #333;
		border-top: 1px solid #ffffff;
		border-right: 5px solid #800000;
		}
		.menu ul ul{
		opacity: 0;
		visibility: hidden;
		transition: all 0.3s;
		position: absolute;
		left: 86%;
		top: -1.5%;
		}
		.menu ul li:hover > ul{
		opacity: 1;
		visibility: visible;
		}
		.menu ul li a{
		color: #ffffff;
		text-decoration: none;
		}
		.menu ul li:hover{
		color: #800000;
	background-color: #800000;
	font-size: 120%;
		}
		</style>
		
	</head>
	
		<body>
		
			<div id="main">
			
			<form method = "post" action = "insertAppraisal.php" style = "margin-left:5%;">
			
			<div class = "eval" style = "height:20%;width:20%;margin-left:70%;position:absolute;text-align:left">
			
			<label><b>Course Code: </b></label>
			<select name="crs" id="crs">
			<option value = "" disabled selected>Please choose...</option>
			</select><br>
			
			<label><b>Class: </b></label>
			<select name = "sec" id = "sec">
			<option value = "" disabled selected>Please choose...</option>
			</select><br>
			
			<label><b>Course Title: </b><span name = "title" id = "title"></label><br>
			<label><b>Class Size: </b><span name = "size" id = "size"></label><br>
			<label><b>Room: </b><span name = "room" id = "room"></label><br>
			<label><b>Date/s: </b><span name = "date" id = "date"></label><br>
			<label><b>Time: </b><span name = "time" id = "time"></label>
			</div>
			
			<div class = "eval" style = "height:15%;width:20%;margin-left:70%;position:absolute;text-align:left;margin-top:22.5%">
			<textarea rows="6" cols="35" name="strength" id = "strength" required pattern="[^'\x22]+" title="Invalid input" 
							placeholder="Enter strengths"></textarea>
							
			</div>
			<div class = "eval" style = "height:15%;width:20%;margin-left:70%;position:absolute;text-align:left;margin-top:34%">
			<textarea rows="6" cols="35" name="recommend" id="recommend" required pattern="[^'\x22]+" title="Invalid input" 
							placeholder="Enter recommendation"></textarea>
			</div>
			
			<div class = "eval" style = "height:58%; margin-left:25%;width: 40%;text-align:left;">
			<label style = "font-family:arial;font-size:20px;">FACULTY CLASSROOM OBSERVATION</label><br><br>
			<table>
				<tr>
					<th style = "color:#000000">A.</th>
					<th style = "text-align:left;color:#000000">Teacher</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
					<th style = "color:#000000">NA</th>
				</tr>
				<tr>
					<td>1.</td>
					<td>Teaching personality</td>
					<td><input type = "radio" name = "qa1" value = "5"></td>
					<td><input type = "radio" name = "qa1" value = "4"></td>
					<td><input type = "radio" name = "qa1" value = "3"></td>
					<td><input type = "radio" name = "qa1" value = "2"></td>
					<td><input type = "radio" name = "qa1" value = "1"></td>
					<td><input type = "radio" name = "qa1" value = "5"></td>
				</tr>
				<tr>
					<td>2.</td>
					<td>Composure</td>
					<td><input type = "radio" name = "qa2" value = "5"></td>
					<td><input type = "radio" name = "qa2" value = "4"></td>
					<td><input type = "radio" name = "qa2" value = "3"></td>
					<td><input type = "radio" name = "qa2" value = "2"></td>
					<td><input type = "radio" name = "qa2" value = "1"></td>
					<td><input type = "radio" name = "qa2" value = "5"></td>
				</tr>
				<tr>
					<td>3.</td>
					<td>Articulation</td>
					<td><input type = "radio" name = "qa3" value = "5"></td>
					<td><input type = "radio" name = "qa3" value = "4"></td>
					<td><input type = "radio" name = "qa3" value = "3"></td>
					<td><input type = "radio" name = "qa3" value = "2"></td>
					<td><input type = "radio" name = "qa3" value = "1"></td>
					<td><input type = "radio" name = "qa3" value = "5"></td>
				</tr>
				<tr>
					<td>4.</td>
					<td>Modulation of voice</td>
					<td><input type = "radio" name = "qa4" value = "5"></td>
					<td><input type = "radio" name = "qa4" value = "4"></td>
					<td><input type = "radio" name = "qa4" value = "3"></td>
					<td><input type = "radio" name = "qa4" value = "2"></td>
					<td><input type = "radio" name = "qa4" value = "1"></td>
					<td><input type = "radio" name = "qa4" value = "5"></td>
				</tr>
				<tr>
					<td>5.</td>
					<td>Mastery of the medium of instruction</td>
					<td><input type = "radio" name = "qa5" value = "5"></td>
					<td><input type = "radio" name = "qa5" value = "4"></td>
					<td><input type = "radio" name = "qa5" value = "3"></td>
					<td><input type = "radio" name = "qa5" value = "2"></td>
					<td><input type = "radio" name = "qa5" value = "1"></td>
					<td><input type = "radio" name = "qa5" value = "5"></td>
				</tr>
				<tr>
					<td>6.</td>
					<td>Mastery of the subject matter</td>
					<td><input type = "radio" name = "qa6" value = "5"></td>
					<td><input type = "radio" name = "qa6" value = "4"></td>
					<td><input type = "radio" name = "qa6" value = "3"></td>
					<td><input type = "radio" name = "qa6" value = "2"></td>
					<td><input type = "radio" name = "qa6" value = "1"></td>
					<td><input type = "radio" name = "qa6" value = "5"></td>
				</tr>
				<tr>
					<td>7.</td>
					<td>Ability to answer questions</td>
					<td><input type = "radio" name = "qa7" value = "5"></td>
					<td><input type = "radio" name = "qa7" value = "4"></td>
					<td><input type = "radio" name = "qa7" value = "3"></td>
					<td><input type = "radio" name = "qa7" value = "2"></td>
					<td><input type = "radio" name = "qa7" value = "1"></td>
					<td><input type = "radio" name = "qa7" value = "5"></td>
				</tr>
				<tr>
					<td>8.</td>
					<td>Openness to student's opinions</td>
					<td><input type = "radio" name = "qa8" value = "5"></td>
					<td><input type = "radio" name = "qa8" value = "4"></td>
					<td><input type = "radio" name = "qa8" value = "3"></td>
					<td><input type = "radio" name = "qa8" value = "2"></td>
					<td><input type = "radio" name = "qa8" value = "1"></td>
					<td><input type = "radio" name = "qa8" value = "5"></td>
				</tr>
			</table>
			</div>
			<!--page2!-->
			<div class = "eval" style = "height:53%;margin-left:25%;width:40%;margin-top:40%; text-align:left;">
			<table>
				<tr>
					<th style = "color:#000000">B.</th>
					<th style = "text-align:left;color:#000000">Teaching Procedure</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
					<th style = "color:#000000">NA</th>
				</tr>
				<tr>
					<td>1.</td>
					<td>Organization of subject matter</td>
					<td><input type = "radio" name = "qb1" value = "5"></td>
					<td><input type = "radio" name = "qb1" value = "4"></td>
					<td><input type = "radio" name = "qb1" value = "3"></td>
					<td><input type = "radio" name = "qb1" value = "2"></td>
					<td><input type = "radio" name = "qb1" value = "1"></td>
					<td><input type = "radio" name = "qb1" value = "5"></td>
				</tr>
				<tr>
					<td>2.</td>
					<td>Ability to relate subject to previous topics</td>
					<td><input type = "radio" name = "qb2" value = "5"></td>
					<td><input type = "radio" name = "qb2" value = "4"></td>
					<td><input type = "radio" name = "qb2" value = "3"></td>
					<td><input type = "radio" name = "qb2" value = "2"></td>
					<td><input type = "radio" name = "qb2" value = "1"></td>
					<td><input type = "radio" name = "qb2" value = "5"></td>
				</tr>
				<tr>
					<td>3.</td>
					<td>Ability to provoke critical thinking</td>
					<td><input type = "radio" name = "qb3" value = "5"></td>
					<td><input type = "radio" name = "qb3" value = "4"></td>
					<td><input type = "radio" name = "qb3" value = "3"></td>
					<td><input type = "radio" name = "qb3" value = "2"></td>
					<td><input type = "radio" name = "qb3" value = "1"></td>
					<td><input type = "radio" name = "qb3" value = "5"></td>
				</tr>
				<tr>
					<td>4.</td>
					<td>Ability to motivate</td>
					<td><input type = "radio" name = "qb4" value = "5"></td>
					<td><input type = "radio" name = "qb4" value = "4"></td>
					<td><input type = "radio" name = "qb4" value = "3"></td>
					<td><input type = "radio" name = "qb4" value = "2"></td>
					<td><input type = "radio" name = "qb4" value = "1"></td>
					<td><input type = "radio" name = "qb4" value = "5"></td>
				</tr>
				<tr>
					<td>5.</td>
					<td>Ability to manage class</td>
					<td><input type = "radio" name = "qb5" value = "5"></td>
					<td><input type = "radio" name = "qb5" value = "4"></td>
					<td><input type = "radio" name = "qb5" value = "3"></td>
					<td><input type = "radio" name = "qb5" value = "2"></td>
					<td><input type = "radio" name = "qb5" value = "1"></td>
					<td><input type = "radio" name = "qb5" value = "5"></td>
				</tr>
				<tr>
					<td>6.</td>
					<td>Questioning techniques</td>
					<td><input type = "radio" name = "qb6" value = "5"></td>
					<td><input type = "radio" name = "qb6" value = "4"></td>
					<td><input type = "radio" name = "qb6" value = "3"></td>
					<td><input type = "radio" name = "qb6" value = "2"></td>
					<td><input type = "radio" name = "qb6" value = "1"></td>
					<td><input type = "radio" name = "qb6" value = "5"></td>
				</tr>
				<tr>
					<td>7.</td>
					<td>Use of teaching aids</td>
					<td><input type = "radio" name = "qb7" value = "5"></td>
					<td><input type = "radio" name = "qb7" value = "4"></td>
					<td><input type = "radio" name = "qb7" value = "3"></td>
					<td><input type = "radio" name = "qb7" value = "2"></td>
					<td><input type = "radio" name = "qb7" value = "1"></td>
					<td><input type = "radio" name = "qb7" value = "5"></td>
				</tr>
			</table>
			</div>
			<div class = "eval" style = "height:23%;margin-left:30%;margin-top:69%;width:30%;text-align:left;">
			
			<table>
				<tr>
					<th style = "color:#000000">C.</th>
					<th style = "text-align:left;color:#000000">Students</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
					<th style = "color:#000000">NA</th>
				</tr>
				<tr>
					<td>1.</td>
					<td>Class attention</td>
					<td><input type = "radio" name = "qc1" value = "5"></td>
					<td><input type = "radio" name = "qc1" value = "4"></td>
					<td><input type = "radio" name = "qc1" value = "3"></td>
					<td><input type = "radio" name = "qc1" value = "2"></td>
					<td><input type = "radio" name = "qc1" value = "1"></td>
					<td><input type = "radio" name = "qc1" value = "5"></td>
				</tr>
				<tr>
					<td>2.</td>
					<td>Class participation</td>
					<td><input type = "radio" name = "qc2" value = "5"></td>
					<td><input type = "radio" name = "qc2" value = "4"></td>
					<td><input type = "radio" name = "qc2" value = "3"></td>
					<td><input type = "radio" name = "qc2" value = "2"></td>
					<td><input type = "radio" name = "qc2" value = "1"></td>
					<td><input type = "radio" name = "qc2" value = "5"></td>
				</tr>
			</table>
				
			</div>
			<div class = "eval" style = "height:50%; margin-left:30%;margin-top:85%;width:30%;text-align:left;">
			
			<table>
				<tr>
					<th style = "color:#000000">D.</th>
					<th style = "text-align:left;color:#000000">General Observation</th>
					<th style = "color:#000000">5</th>
					<th style = "color:#000000">4</th>
					<th style = "color:#000000">3</th>
					<th style = "color:#000000">2</th>
					<th style = "color:#000000">1</th>
					<th style = "color:#000000">NA</th>
				</tr>
				<tr>
					<td>1.</td>
					<td>Rapport between teacher and students</td>
					<td><input type = "radio" name = "qd1" value = "5"></td>
					<td><input type = "radio" name = "qd1" value = "4"></td>
					<td><input type = "radio" name = "qd1" value = "3"></td>
					<td><input type = "radio" name = "qd1" value = "2"></td>
					<td><input type = "radio" name = "qd1" value = "1"></td>
					<td><input type = "radio" name = "qd1" value = "5"></td>
				</tr>
				<tr>
					<td>2.</td>
					<td>Class atmosphere</td>
					<td><input type = "radio" name = "qd2" value = "5"></td>
					<td><input type = "radio" name = "qd2" value = "4"></td>
					<td><input type = "radio" name = "qd2" value = "3"></td>
					<td><input type = "radio" name = "qd2" value = "2"></td>
					<td><input type = "radio" name = "qd2" value = "1"></td>
					<td><input type = "radio" name = "qd2" value = "5"></td>
				</tr>
				<tr>
					<td>3.</td>
					<td>Overall teacher impact</td>
					<td><input type = "radio" name = "qd3" value = "5"></td>
					<td><input type = "radio" name = "qd3" value = "4"></td>
					<td><input type = "radio" name = "qd3" value = "3"></td>
					<td><input type = "radio" name = "qd3" value = "2"></td>
					<td><input type = "radio" name = "qd3" value = "1"></td>
					<td><input type = "radio" name = "qd3" value = "5"></td>
				</tr>
				<tr>
					<td>4.</td>
					<td>Overall classroom condition</td>
					<td><input type = "radio" name = "qd4" value = "5"></td>
					<td><input type = "radio" name = "qd4" value = "4"></td>
					<td><input type = "radio" name = "qd4" value = "3"></td>
					<td><input type = "radio" name = "qd4" value = "2"></td>
					<td><input type = "radio" name = "qd4" value = "1"></td>
					<td><input type = "radio" name = "qd4" value = "5"></td>
				</tr>
			</table>
				
			</div>
			<button class="sign" style = "margin-left:43%;margin-right:25%;width:10%;margin-top:115%;position:absolute" name = "observeVice">
				<span>Submit </span></button>
				</form>
				
				<div class = "eval" style = "margin-top:45%;height:35%;margin-left:78%;width:15%;text-align:left">
				
			<label>RATING INTERPRETATION:</label><br><br>
<label>5 = Excellent</label><br><br>
<label>4 = Superior, Very Good</label><br><br>
<label>3 = Good</label><br><br>
<label>2 = Fair</label><br><br>
<label>1 = Poor or Unsatisfactory</label><br><br>
<label>NA = Not Applicable</label>			
			
			</div>
			
			<header>
			
				<div class = "container">
				
					<div class = "asd">
					<label id = "name" style = "position:fixed;margin-top:3%;margin-left:50%;font-family:arial;color:#ffffff;font-size:20px;"></label>
						<div id="mySidenav" class="sidenav">
						
							<img src="background.jpg" height = "100" width = "100" class = "picture" style="cursor:pointer">
							<a href = "vicedean.php" style = "font-size:15px">Home</a>
							<!--<a href = "profile.php" style = "font-size:15px">Profile</a>!-->
							<a href = "appraisalVice.php" style = "font-size:15px">Faculty Performance Appraisal</a>
							<a href = "deptappraisalVice.php" style = "font-size:15px">Department Chair Performance Appraisal</a>
							<a href = "observeVice.php" style = "font-size:15px">Faculty Classroom Observation</a>
							<a href = "deptobserveVice.php" style = "font-size:15px">Department Chair Classroom Observation</a>
							<a href = "stats.php" style = "font-size:15px">Statistics</a>
							<!--<a href = "stats.php" style = "font-size:15px">Department Chair Statistics</a>!-->
							<!--<a href = "settings.php" style = "font-size:15px">Settings</a>!-->
							<a onclick = "document.getElementById('id02').style.display='block'" style = "cursor:pointer;font-size:15px">Logout</a>
						
						</div>
						
						<img src="btnNav.jpg" alt="Navigation" height = "85dp" class = "menu" onclick="openNav()" style="cursor:pointer">
		
						<a href = "account.php"><img src="logo.png" alt="San Beda College Alabang" height = "70dp" class = "logo" style="cursor:pointer"></a>
					
						<font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">San Beda College Alabang</font>
						
						<div class = "menu" style = "margin-left:-3%;margin-top:6.7%;height:100%;width:20%;
			font-family:arial;font-size:20px;float:left;position:absolute">
						
						<ul>
						<?php
						$course = "select * from tblcor group by department asc";
						$rescourse = mysqli_query($conn,$course);
						while($rowcourse = mysqli_fetch_assoc($rescourse)){
							
							$sql1 = "select * from tblcor where department = '".$rowcourse['department']."' and not facultyID = '".$_SESSION['prof']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."' group by facultyID";
							$select1 = mysqli_query($conn,$sql1);
							$row1 = mysqli_fetch_assoc($select1);
							
							if($row1 < 1){
							}else{
							echo "<li><a href = '#'>".$row1['department']."</a> <ul id = 'sample' name = 'sample'>";
							
							$sql = "select * from tblcor where department = '".$rowcourse['department']."' and schlyr = '".$_SESSION['yr']."' and sem = '".$_SESSION['sem']."' group by facultyID";
							$select = mysqli_query($conn,$sql);
							while($row = mysqli_fetch_assoc($select)){
							$query = "select * from tblprofessor where facultyID = '".$row['facultyID']."'";
							$res = mysqli_query($conn,$query);
							$row1 = mysqli_fetch_assoc($res);
							
							$user = "select * from tbluser where userID = '".$row1['userID']."' and not role = 'dept.chair'";
							$result = mysqli_query($conn,$user);
							$row2 = mysqli_fetch_assoc($result);
							
							if($row2 < 1){
							}else{
								echo "<li value = '".$row['facultyID']."'><a href = '#'>".$row2['firstname']." ".$row2['lastname']."</a></li>";
							}
							
							}
							echo "</ul></li>";
							}

							

							
						}
						?>
						</ul>
						
						</div>
						

					</div>
					
						<nav class = "nav">
							<ul class = "ul">
								<li class = "li"><a class = "a" href = "#"><?php echo $_SESSION['firstname'] ?></a></li>
							</ul>
						</nav>
					
				</div>
				
			</div>
			
			</header>
	
			<footer class = "footerhome" style = "bottom:-200%">
			
			
				<div class = "follow">
				
					<p class = "bottom">FOLLOW US</p>
					
					<ul class = "link">
					
						<li><a href="https://www.google.com.ph/?gfe_rd=cr&dcr=0&ei=12WNWqO6G9GiX_j3gqgJ" target="_blank">
							<image src = "google.png" height = "10"> Google+</a></li><br>
						
						<li><a href="https://twitter.com/sbcasec?lang=en" target="_blank">
							<image src = "twitter.png" height = "10"> Twitter</a></li><br>
							
						<li><a href="https://www.facebook.com/BedaAlabang/" target="_blank">
							<image src = "facebook.png" height = "10"> Facebook</a></li><br>
							
						<li><a href="https://www.instagram.com/?hl=en" target="_blank">
							<image src = "instagram.png" height = "10"> Instagram</a></li><br>
							
						<li><a href="https://www.youtube.com" target="_blank"><image src = "youtube.png" height = "10"> Youtube</a></li>
					
					</ul>
					
				</div>
				
				<div class = "contactUs">
				
					<p class = "bottom">CONTACT US</p>
					
					<ul class = "link">
					
						<li><a class = "aaaa" href="http://www.sanbeda-alabang.edu.ph/bede/" target="_blank">
							Website: http://www.sanbeda-alabang.edu.ph</a></li><br>
							
						<li><a class = "aaaa" href="#home" style = "cursor:auto">E-mail: sbca@sanbeda-alabang.edu.ph</a></li><br>
						
						<li><a class = "aaaa" href="#home" style = "cursor:auto">Telephone: 236-7222</a></li><br>
					
					</ul>
					
				</div>
				
				<div class = "copyright">
					<p id = "copyright"/>
				</div>
	
			</footer>
			
	<script>
		var d = new Date().getFullYear();
		document.getElementById("copyright").innerHTML = "Copyright &copy; " + d + " San Beda College Alabang, All Rights Reserved.";
	</script>
	
			<div id="id02" class="logout">
  
				<form class="logout-content animate" method = "post" action = "includes/logout.php">
					
					<div class="imgcontainerlogout">
						<span onclick="document.getElementById('id02').style.display='none'" class="closelogout" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogout">
						<label for="username"><b class = "align">Are you sure?</b></label><br>
						<button class="button1" name = "logout"><span>Logout </span></button>
					</div>
			</div>

	<script>
	
		var logout = document.getElementById('id02');
		window.onclick = function(event) {
			if (event.target == logout) {
				logout.style.display = "none";
			}
		}
		
		var x = true;
		function openNav() {
			if(x == true) {
				document.getElementById("mySidenav").style.width = "197px";
				document.getElementById("main").style.marginLeft = "197px";
				x = false;
			}
			else {
				closeNav();
				x = true;
			}
		}

		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
			document.getElementById("main").style.marginLeft= "0";
			document.body.style.backgroundColor = "white";
		}
		
	</script>

		</body>
		
</html>