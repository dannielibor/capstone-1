<?php
session_start();
include_once 'includes/db.inc.php';
?>
<!DOCTYPE html>
<html>

	<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="style.css">
		<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="js-post.js"></script>
		<title>SBCA - Evaluation Form</title>
		
		<style>
		
		textarea{
		resize:none;
		}
		
		.comment{
		width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
	text-align: left;
	font-family: Arial;
	font-size: 15px;	
		}
		
* {box-sizing: border-box;}
body {font-family: Verdana, sans-serif;}
.mySlides {display: none;}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  width:100%;
  position: absolute;
  margin: auto;
}

.active {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .text {font-size: 11px}
}
</style>
		
	</head>
	
		<body>
		
		
		<div class="slideshow-container">
		
		<?php
		
		$sql = "select * from tblbanners";
		$result = mysqli_query($conn,$sql);
		$row = mysqli_fetch_assoc($result);
		if($row < 1){
			echo "<div class='mySlides fade'>
  <img src='background.jpg' style='width:100%'>
</div>";
		}else{
			while($row = mysqli_fetch_assoc($result)){
				echo '<div class="mySlides fade">
  <img src="data:image/jpeg;base64,'.base64_encode($row['picture'] ).'" style="width:100%">
</div>';
			}
		
		}
		
		?>

</div>

<script>
var slideIndex = 0;
showSlides();

function showSlides() {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    for (i = 0; i < slides.length; i++) {
       slides[i].style.display = "none";  
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1}   
    slides[slideIndex-1].style.display = "block";  
    setTimeout(showSlides, 5000); // Change image every 2 seconds
}
</script>
		
		
		<div style = "background-color:#fff;position:absolute;width:100%;height:80%;top:100%;text-align:center">
		
				
					<ul style = "margin-top: 1%; margin-right: 50px">
						<li style = "list-style-type: none"><image src = "mission.png" alt = "our mission" height = "100" align = "center"></li>
							<p style = "font-style: italic; font-family: arial">To provide excellent and responsive programs and services, adopt empowering management system, and build a learning, caring, and praying community guided by the teachings of St. Benedict and the example of St. Bede.</p>
							
						<li style = "list-style-type: none"><image src = "vision.png" alt = "our mission" height = "80" align = "center"></li>
							<p style = "font-style: italic; font-family: arial">To be a leading Catholic Christian educational institution committed to the holistic formation of persons who excel in their respective endeavors and are guided by the Benedictine principles of Prayer, Work and Peace.  </p>	
						
						<li style = "list-style-type: none"><image src = "values.png" alt = "our mission" height = "90" align = "center"></li>
							<li style = "list-style-type: none; margin-top: 10px"><image src = "prayer.png" alt = "prayer" height = "80"><image src = "work.png" alt = "work" height = "80">
								<image src = "peace.png" alt = "peace" height = "80"><image src = "community.png" alt = "community" height = "80">
								<image src = "service.png" alt = "service" height = "80">
					</ul>
				
		
		</div>
		
		<div style = "font-family: Arial;
	font-size: 15px;
	word-wrap: break-word;
	opacity: 0.9;
    padding: 20px; 
	text-align: left;
	background-color:#D3D3D3;
	position:absolute;
	width:100%;
	height:60%;
	top:180%;">
			
					<ul class = "contacts">
					
						<li><a class = "bbb" href="#"><image src = "contact.png" alt="236-7222" height = "30"> 236-7222</a></li><br>
						
						<li><a class = "bbb" href="https://twitter.com/sbcasec?lang=en" target="_blank">
							<image src = "email.png" height = "30"> sbca@sanbeda-alabang.edu.ph</a></li><br>
							
						<li><a class = "bbb" href="http://www.sanbeda-alabang.edu.ph/bede/" target="_blank">
							<image src = "link.png" height = "30"> www.sanbeda-alabang.edu.ph</a></li>
					
					</ul>
		
						<ul class = "media" style = "margin-top:-10%;margin-right:1%">
							<li><a href="https://www.facebook.com/BedaAlabang/" target="_blank" class="facebookBtn smGlobalBtn"></a></li><br>
							<li><a href="https://twitter.com/sanbeda_alabang" target="_blank" class="twitterBtn smGlobalBtn"></a></li><br>
						</ul>
					
					<p style = "text-align: center;margin-top: 16%;margin-left:3%">8 Don Manolo Bldg., Alabang Hills Village, Muntinlupa City</p>
			
		</div>
			
				<form action="mailto:sbca@sanbeda-alabang.edu.ph" method="post" enctype="text/plain" class = "mail" 
					style = "margin-left: 35%;top:180%">
					
						<input type="name" class = "comment" name="name" placeholder="Full name" size = "30" required pattern = "[a-z,A-Z]{1,15}" 
							title="Name should only contain letters. e.g. John Joseph"><br><br>
							
						<input type="email" class = "comment" name="mail" placeholder="E-mail" size = "30" required 
							pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="E-mail invalid. e.g. john@example.com"><br><br>
							
						<textarea rows="5" class = "comment" maxlength = "100" cols="50" name="comment" form="usrform" required pattern="[^'\x22]+" title="Invalid input" 
							placeholder="Comment"></textarea><br><br>
							
					<input type="submit" value="Send" class = "button" style = "cursor:pointer;margin-top:-23%;margin-left:40%;width:20%">
					
				</form>
		
				<header>
				<!--contains header!-->
					<div class = "container">
					
						<div class = "asd">
							<!--sbca logo!-->
							<a href = "home.php"><img src="logo.png" alt="San Beda College Alabang" height = "70dp" 
									class = "logo" style="cursor:pointer"></a>
		
							<font face="Old English Text MT" size="6" color = "#ffffff" class = "title" 
									style="cursor:default">San Beda College Alabang</font>
						
						</div>
						
							<nav class = "nav" style = "margin-left:20%"><!--tabs!-->
								<ul class = "ul">
									<li class = "li"><a class = "a" href = "#"
										onclick = "document.getElementById('id01').style.display='block'">Login</a></li>
										
									<li class = "li"><a class = "a" href = "#" style = "margin-left: 20px"
										onclick = "document.getElementById('id02').style.display='block'">Sign up</a></li>
								</ul>
							</nav> 
							
					</div><!--end of header!-->
					
				</header>
				
			<footer class = "footerhome" style = "top:35%"><!--footer!-->
			
				<div class = "follow">
				
					<p class = "bottom">FOLLOW US</p>
					
						<ul class = "link">
						
							<li><a href="https://www.google.com.ph/?gfe_rd=cr&dcr=0&ei=12WNWqO6G9GiX_j3gqgJ" target="_blank">
								<image src = "google.png" height = "10"> Google+</a></li><br>
								
							<li><a href="https://twitter.com/sbcasec?lang=en" target="_blank">
								<image src = "twitter.png" height = "10"> Twitter</a></li><br>
								
							<li><a href="https://www.facebook.com/BedaAlabang/" target="_blank">
								<image src = "facebook.png" height = "10"> Facebook</a></li><br>
								
							<li><a href="https://www.instagram.com/?hl=en" target="_blank">
								<image src = "instagram.png" height = "10"> Instagram</a></li><br>
								
							<li><a href="https://www.youtube.com" target="_blank">
								<image src = "youtube.png" height = "10"> Youtube</a></li>
								
						</ul>
				</div>
				
				<div class = "contactUs">
				
					<p class = "bottom">CONTACT US</p>
					
						<ul class = "link">
						
							<li><a class = "aaaa" href="http://www.sanbeda-alabang.edu.ph/bede/" target="_blank">
								Website: http://www.sanbeda-alabang.edu.ph</a></li><br>
								
							<li><a class = "aaaa" href="#home" style = "cursor:auto">E-mail: sbca@sanbeda-alabang.edu.ph</a></li><br>
							
							<li><a class = "aaaa" href="#home" style = "cursor:auto">Telephone: 236-7222</a></li><br>
						
						</ul>
						
				</div>
				
				<div class = "copyright">
					<p id = "copyright"/>
				</div>
	
			</footer><!--end of footer!-->
	
	<script><!--get current year!-->
		var d = new Date().getFullYear();
		document.getElementById("copyright").innerHTML = "Copyright &copy; " + d + " San Beda College Alabang, All Rights Reserved.";
	</script>
	
			<div id="id01" class="login"><!--log in form!-->
			
				<form class="login-content animate" method = "post" action = "includes/login.php"><!--pop up form!-->
				
					<div class="imgcontainerlogin"><!--close form!-->
						<span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
					</div>

					<div class="containerlogin"><!--content of login form!-->
					
						<input type="text" class = "text" placeholder="Username" name="username" required pattern = "[a-z,A-Z,0-9]{1,15}" 
								title="Username should only contain letters or numbers. e.g. John02">

						<input type="password" class = "passwordlogin" placeholder="Password" name="password" required pattern = "[a-z,A-Z,0-9]{1,15}"
							title="Password should only contain letters or numbers. e.g. John02">
							
						<button class="button" name = "login"><span>Login </span></button>
						
					</div>
					
				</form>
				
			</div><!--end of login form!-->
			
			<div id="id02" class="signup"><!--choose sign up form!-->
  
				<div class="signup-content animate"><!--pop up form!-->
				
					<div class="imgcontainersignup"><!--close form!-->
						<span onclick="document.getElementById('id02').style.display='none'" class="closesign" title="Close Modal">&times;</span>
					</div>

					<div class="containersignup">
					
					<div class = "one"><!--choose student!-->
						<image src = "student.png" height = "100" width = "100" style = "margin-bottom:250px;margin-left:45px"><!--icon!-->
						<p style = "padding:10px;margin-top: -200px;text-align:center; color:#ffffff; margin-bottom: 130px;">
						Create an account as student. Allows you to evaluate your professor/s.</p><!--description!-->
						<button class="student" name = "student" onclick = "document.getElementById('student').style.display='block';
								document.getElementById('id02').style.display='none'">
							<span>Student </span>
						</button><!--launch sign up form!-->
					</div>	
					
					<div class = "two"><!--professor!-->
						<image src = "prof.png" height = "100" width = "100" style = "margin-bottom:250px;margin-left:45px"><!--icon!-->
						<p style = "padding:10px;margin-top: -200px;text-align:center; color:#ffffff; margin-bottom: 130px;">
						Create an account as professor. Allows you to evaluate faculty.</p><!--description!-->
						<button class="professor" name = "prof" onclick = "document.getElementById('professor').style.display='block';
								document.getElementById('id02').style.display='none'">
							<span>Professor </span>
						</button><!--launch sign up form!-->
					</div>	
					
					</div>
					
				</div>
				
			</div>
			
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
			
			<div id="student" class="signup_student"><!--student sign up form!-->
			
			<form class="signup_student-content animate" method = "post" action = "includes/signup.php" enctype="multipart/form-data"><!--pop up student sign up form!-->
				
					<div class="imgcontainersignup_student">
					
						<div class = "tooltip">
						
							<div id="profile-container">
							
								<image id="profileImage" src="default.png" title = "profile picture" /><!--default profile pic!-->
								
							</div>
							<!--open dialog!-->
							<input id="imageUpload" type="file" name="profile_photo"/>
							
							<span class="tooltiptext">Insert profile picture</span>
							
						</div>
						<!--close form!-->
						<a href = "home.php"><span onclick="document.getElementById('student').style.display='none'" 
						class="closesign" title="Close Modal">&times;</span></a>
						
					</div>

					<div class="containersignup_student"><!--content of student sign up form!-->
					
						<input type="firstname" placeholder="First Name" name="firstname" required pattern = "[a-z,A-Z]{1,15}" 
								title="First name should only contain letters. e.g. John">

						<input type="lastname" placeholder="Last Name" name="lastname" required pattern = "[a-z,A-Z]{1,15}"
							title="Last name should only contain letters. e.g. Bautista">
							<br>
						<input placeholder="E-mail"  type="email" name="email" class = "email" required style = "width:97%">
						<br>
						<input type="user" placeholder="Username" name="user" required pattern = "[a-z,A-Z,0-9]{1,15}"
							title="Username should only contain letters or numbers. e.g. John02">
						
						<input type="studentno" placeholder="Student ID" name="studentno" required pattern = "[0-9]{1,15}"
							title="Student number should only contain numbers. e.g. 2015300332"><br>

						<input type="password" id = "password" class = "passwordsignup" placeholder="Password" name="password" required pattern = "[a-z,A-Z,0-9]{1,15}"
							title="Password should only contain letters or numbers. e.g. john02">
						
						<input type="password" id = "confirm" class = "passwordsignup" placeholder="Confirm Password" name="confirm" required pattern = "[a-z,A-Z,0-9]{1,15}"
							title="Password should only contain letters or numbers. e.g. john02"><br>
						
						<div class = "show">
							<input type="checkbox" onclick="myFunction()">Show Password <br>
						</div>
						
						<script>
						var password = document.getElementById("password");
							var confirm = document.getElementById("confirm");
							function myFunctionProf() {
							if (password.type === "password") {
								password.type = "confirm";
							} else {
								password.type = "password";
							}
							if (confirm.type === "password") {
								confirm.type = "confirm";
							} else {
								confirm.type = "password";
							}
							}
							
							function validatePassword(){
								if(password.value != confirm.value){
									confirm.setCustomValidity("Password Don't Match");
								}
								else{
									confirm.setCustomValidity(''); 
								}
							}
							
							password.onchange = validatePassword;
							confirm.onkeyup = validatePassword;
							
						</script>
						
					<div align = "center">
						
						<label for = "gender"><b>Gender: </b></label>
								<input type = "radio" name = "gender" style = "cursor:pointer" value="Male">Male</input>
								<input type = "radio" name = "gender" style = "cursor:pointer" value="Female">Female</input>
						
					</div>
					
					<button class="sign" name = "studsign"><span>Sign up </span></button>
					
					</div>
				
				</form>

			</div><!--end of student sign up form!-->	
			
			<div id="professor" class="signup_student"><!--professor sign up form!-->
			
			<form class="signup_student-content animate" method = "post" action = "includes/signupProf.php" enctype="multipart/form-data"><!--pop up professor sign up form!-->
				
					<div class="imgcontainersignup_student">
					
						<div class = "tooltip">
						
							<div id="profile-containerProf">
							
								<image id="profileImageProf" src="default.png" title = "profile picture" /><!--default profile pic!-->
								
							</div>
							<!--open dialog!-->
							<input id="imageUploadProf" type="file" name="profile_photoProf" placeholder="Photo" required />
							
							<span class="tooltiptext">Insert profile picture</span>
							
						</div>
						<!--close form!-->
						<a href = "home.php"><span onclick="document.getElementById('professor').style.display='none'" 
						class="closesign" title="Close Modal">&times;</span></a>
						
					</div>

					<div class="containersignup_student"><!--content of student sign up form!-->
					
						<input type="firstname" placeholder="First Name" name="proffirstname" required 
								title="First name should only contain letters. e.g. John">

						<input type="lastname" placeholder="Last Name" name="proflastname" required
							title="Last name should only contain letters. e.g. Bautista">
							<br>
						<input placeholder="E-mail"  class = "email" type="email" name="profemail" required>
						
						<select class = "email" id = "post" name = "post">
						<option disabled selected value = "">Please choose...</option>
						<option value = "Atty.">Atty.</option>
						<option value = "Dr.">Dr.</option>
						<option value = "Engr.">Engr.</option>
						<option value = "Prof.">Prof.</option>
						</select>
						
						<br>
						<input type="user" placeholder="Username" name="profuser" required pattern = "[a-z,A-Z,0-9]{1,15}"
							title="Username should only contain letters or numbers. e.g. John02">
						
						<input type="studentno" placeholder="Faculty ID" name="profno" required pattern = "[0-9]{1,15}"
							title="Student number should only contain numbers. e.g. 2015300332"><br>

						<input type="password" id = "profpassword" class = "passwordsignup" placeholder="Password" name="profpass" required pattern = "[a-z,A-Z,0-9]{1,15}"
							title="Password should only contain letters or numbers. e.g. john02">
						
						
						<input type="password" id = "profconfirm" class = "passwordsignup" placeholder="Confirm Password" name="profconfirm" required pattern = "[a-z,A-Z,0-9]{1,15}"
							title="Password should only contain letters or numbers. e.g. john02"><br>
						
						<div class = "show">
							<input type="checkbox" onclick="myFunctionProf()">Show Password <br>
						</div>
						
						<script>
						var profpassword = document.getElementById("profpassword");
							var profconfirm = document.getElementById("profconfirm");
							function myFunctionProf() {
							if (profpassword.type === "profpassword") {
								profpassword.type = "profconfirm";
							} else {
								profpassword.type = "profpassword";
							}
							if (profconfirm.type === "profpassword") {
								profconfirm.type = "profconfirm";
							} else {
								profconfirm.type = "profpassword";
							}
							}
							function profvalidatePassword(){
								if(profpassword.value != profconfirm.value){
									profconfirm.setCustomValidity("Password Don't Match");
								}
								else{
									profconfirm.setCustomValidity(''); 
								}
							}
							
							profpassword.onchange = profvalidatePassword;
							profconfirm.onkeyup = profvalidatePassword;
						</script>
					
					<div align = "center">
						
						<label for = "profgender"><b>Gender: </b></label>
								<input type = "radio" name = "profgender" style = "cursor:pointer" value="Male">Male</input>
								<input type = "radio" name = "profgender" style = "cursor:pointer" value="Female">Female</input>
						
					</div>
					
					<button class="sign" type="submit" name = "Profsignup"><span>Sign up </span></button>
					
					</div>
				
				</form>

			</div><!--end of professor sign up form!-->

						<script><!--preview picture!-->
							$("#profileImage").click(function(e) {
							$("#imageUpload").click(); });
							
							$("#profileImageProf").click(function(e) {
							$("#imageUploadProf").click(); });
							
							$("#profileImageVice").click(function(e) {
							$("#imageUploadVice").click(); });
							
							$("#profileImageDean").click(function(e) {
							$("#imageUploadDean").click(); });

							function fasterPreview_student( uploader ) {
								if ( uploader.files && uploader.files[0] ){
									$('#profileImage').attr('src', 
									window.URL.createObjectURL(uploader.files[0]) );
								}
							}
							
							function fasterPreview_prof( uploader ) {
								if ( uploader.files && uploader.files[0] ){
									$('#profileImageProf').attr('src', 
									window.URL.createObjectURL(uploader.files[0]) );
								}
							}
							
							function fasterPreview_vice( uploader ) {
								if ( uploader.files && uploader.files[0] ){
									$('#profileImageVice').attr('src', 
									window.URL.createObjectURL(uploader.files[0]) );
								}
							}
							
							function fasterPreview_dean( uploader ) {
								if ( uploader.files && uploader.files[0] ){
									$('#profileImageDean').attr('src', 
									window.URL.createObjectURL(uploader.files[0]) );
								}
							}

							$("#imageUpload").change(function(){
							fasterPreview_student( this ); });
							
							$("#imageUploadProf").change(function(){
							fasterPreview_prof( this ); });
							
							$("#imageUploadVice").change(function(){
							fasterPreview_vice( this ); });
							
							$("#imageUploadDean").change(function(){
							fasterPreview_dean( this ); });
						</script>			
				
	<script>
		<!--date today!-->
		var d = new Date();
		document.getElementById("today").innerHTML = d.toDateString();
		<!--school year!-->
		
		var login = document.getElementById('id01');
		var signup = document.getElementById('id02');
		var student = document.getElementById('student');
		var professor = document.getElementById('professor');
		
		window.onclick = function(event) {
			
			if (event.target == signup) {
				signup.style.display = "none";
			}
			if (event.target == login) {
				login.style.display = "none";
			}
			if (event.target == student) {
				student.style.display = "none";
			}
			if (event.target == professor) {
				professor.style.display = "none";
			}
		}
		
	</script>
	
	<script><!--side menu!-->
	
		var x = true;
		
		function openNav() {
			if(x == true) {
				document.getElementById("mySidenav").style.width = "197px";
				document.getElementById("main").style.marginLeft = "197px";
				x = false;
			}
			else {
				closeNav();
				x = true;
			}
		}

		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
			document.getElementById("main").style.marginLeft= "0";
			document.body.style.backgroundColor = "white";
		}
		
	</script>
	
		</body>
		
</html>